

** v-0.1 2014.04.08
- 修改短关键字为string类型，可使用多个字符作为短关键字
- 增加SetUniqArgs设置短关键字只能唯一出现的关键字
- 增加SetMutualArgs设置不能同时出现的短关键字，使用冒号(:)区分多个短关键字
- 增加函数SetNecessaryArgs设置必须要有的短关键字

