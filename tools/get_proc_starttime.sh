#!/bin/bash
################################################################################
#  Copyright (C) 2015-2018 IQIYI All rights reserved.
# 
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Thu 20 Dec 2018 11:34:40 AM CST
#  Destription: 获取进程的启动时间
#    100 可通过`getconf CLK_TCK`获取到
#
################################################################################
cd `dirname $0`

UPTIME=""   #启动时间
pid=$1
function GetProcStartTime() {
    local PID=$1
    if [ -z ${PID} ]
    then
        echo "usage ${0} pid"
        return 0    
    fi
    local STARTTIME=$(awk '{print int($22 /100)}' /proc/$PID/stat)
    local UT=$(awk '{print int($1)}' /proc/uptime)
    local NOW=$(date +%s)
    local DIFF=$((NOW - (UT - STARTTIME)))
    UPTIME=`date -d @$DIFF`
}

GetProcStartTime ${pid}
echo "start time : ${UPTIME}"
