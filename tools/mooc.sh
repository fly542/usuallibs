#!/bin/bash
# 
#!/bin/bash
################################################################################
#  Copyright (C) 2024 IQIYI All rights reserved.
#
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Sun 29 Sep 2024 04:36:03 PM CST
#  Destription:  此脚本可以将移动端的2中格式的mooc文件转为mp4
#
#
# 总的处理原则:
# 方法1: 处理视频文件为Moc_1473179513_1296627876_1格式的视频文件
#  		ffmpeg.exe -i Moc_1473179513_1296627876_1 -c:v copy -an 1.mp4
# 方法2: 处理视频文件目录为Moc_1473179513_1296627876_1,其下为ts和m3u8的文件的格式
# 		ffmpeg -i "$file" -c copy "${formatted_index}.ts"
#       ffmpeg -i "${formatted_index}.ts" -c copy "${formatted_index}.mp4"
#
# 手机端缓存文件位置: 此电脑\Redmi K40\内部存储设备\Android\data\com.netease.edu.ucmooc\files\xcache\
#
################################################################################

cd `dirname $0`

# 设置要遍历的目录和目标目录
source_dir="/path/to/source"
target_dir="/path/to/target"
prefix="prefix-"  # 设置文件名前缀

# 调用的方法1或2，1为单个文件,2为目录模式
method=1

# 创建目标目录（如果不存在）
mkdir -p "$target_dir"

# 方法1：处理文件并转换格式
process_moc_files() {
    local source="$1"
    local target="$2"
    local prefix="$3"

    # 初始化文件序号
    local index=1

    # 遍历指定目录下的所有文件
    for file in "$source"/*; do
        if [ -f "$file" ]; then
            echo "Processing file $index: $(basename "$file")"

            # 格式化序号为两位数
            local formatted_index=$(printf "%02d" "$index")

            # 使用 ffmpeg 将文件转换为 .mp4
            ffmpeg -i "$file" -c:v copy -an "$target/${prefix}${formatted_index}.mp4"

            echo "File $index processed successfully: $target/${prefix}${formatted_index}.mp4"

            # 递增文件序号
            ((index++))
        fi
    done
    # 处理结束
    echo "Processing completed."
}

# 方法2 
process_moc_dirs() {
    local source="$1"
    local target="$2"
    local prefix="$3"
	
    # 初始化文件序号
    local index=1

	# 进入ts父目录
	cd "$source"
	# 遍历当前目录下的所有子目录
	for dir in */; do
		# 确保是一个目录
		if [ -d "$dir" ]; then
			echo "Processing directory #$index: $dir"
	
			# 进入子目录
			cd "$dir" || exit
	
			# 查找并处理 .m3u8 文件
			for file in *.m3u8; do
				if [ -f "$file" ]; then
					# 格式化序号为两位数
					formatted_index=$(printf "%02d" "$index")
	
					# 使用 ffmpeg 将 .m3u8 文件转换为 .ts 文件
					ffmpeg -i "$file" -c copy "${formatted_index}.ts"
	
					# 将生成的 .ts 文件转换为 .mp4 文件
					ffmpeg -i "${formatted_index}.ts" -c copy "${formatted_index}.mp4"
	
					# 拷贝 .mp4 文件到指定目录，并加上前缀
					cp "${formatted_index}.mp4" "${target}/${prefix}${formatted_index}.mp4"
	
					echo "Directory #$index processed successfully, copied to target directory as aaa${formatted_index}.mp4."
					
					# 删除生成的 .ts 和 .mp4 文件
					rm -f "${formatted_index}.ts" "${formatted_index}.mp4"
				else
					echo "No .m3u8 files found in $dir"
				fi
			done
	
			# 返回上一级目录
			cd ..
	
			# 递增目录序号
			index=$((index + 1))
		fi
	done
}

# 调用方法
if [ "$method" -eq 1 ]; then
	process_moc_files "$source_dir" "$target_dir" "$prefix"
elif [ "$method" -eq 2 ]; then
	process_moc_dirs "$source_dir" "$target_dir" "$prefix"
else
	echo "Unknown method"
fi
