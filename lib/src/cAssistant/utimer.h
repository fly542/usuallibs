#ifndef UTIMER_H
#define UTIMER_H

/* purpose @ 毫秒计时器
 * date    @ 2014.09.03
 * author  @ haibin.wang
 */

#include <sys/time.h>

/* pur @ 毫秒计时器,可重复调用Start和StopX重复利用一个Timer对象
 * usage @
 *      UTimer timer;
 *      timer.Start();
 *      long long costU = timer.StopU();
*/
class UTimer {
    private:
    struct timeval m_begin, m_end;

    public:
    UTimer();
    virtual ~UTimer();

    public:
    /* pur @ 开始计时器
    */
    void Start();

    /* pur @ 停止计时器并返回计时器运行时间
     * return @ 返回计时时间值，返回值时间单位：微秒
    */
    long long StopU();

    /* pur @ 停止计时器并返回计时器运行时间
     * return @ 返回计时时间值，返回值时间单位：毫秒
    */
    long long StopM();

    /* pur @ 停止计时器并返回计时器运行时间
     * return @ 返回计时时间值,返回值时间单位：秒
    */
    long long StopS();
};

#endif
