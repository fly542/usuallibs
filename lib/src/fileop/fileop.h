/* purpose @ 文件操作函数，对象销毁的时候会自动关闭打开的文件句柄
 * date    @
 * author  @ haibin.wang
 */

#ifndef FILEOP_H
#define FILEOP_H
#include <string>

class FileOp {
    public:
    FileOp();
    ~FileOp();

    /* pur @ 打开文件
     * para @ filePath 要打开的文件句柄
     * para @ oflag 打开的模式
     * return @ true 打开成功，false 打开失败
    */
    bool Open(const char *filePath, int oflag);

    /* pur @ 关闭打开的文件句柄
    */
    void Close();

    /* pur @ 写入数据
     * para @ data 要写入的数据
     * para @ nbyte 要写入的长度
     * return @
    */
    void WriteLine(const char *data, size_t nbyte, bool bLine);

    /* pur @ 写入数据
     * para @ data 要写入的数据
     * return @
    */
    void WriteLine(const std::string &data);

    private:
    FileOp(const FileOp &)
    {
    }
    FileOp &operator=(const FileOp &){};

    private:
    //文件句柄
    int m_fileHandle;
};

#endif
