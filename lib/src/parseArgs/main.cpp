#include "parsingargs.h"
#include <getopt.h>
#include <iostream>
#include <string.h>

using namespace std;

int main(int argc, char *argv[])
{

    // string tmpPara = "-p \"4567\" --out 1.log "; //ok
    // string tmpPara = "xxxx -p \"4567\" --out 1.log ";//ok
    // string tmpPara = "-p \"4567\" --out 1.log 2.log"; //ok
    std::map<std::string, std::vector<std::string> > result;
    std::string errPos;
    ParsingArgs pa;
    pa.AddArgType("h", "host", ParsingArgs::MUST_VALUE);        // ip
    pa.AddArgType("p", "port", ParsingArgs::MUST_VALUE);        //端口
    pa.AddArgType("help", "help", ParsingArgs::NO_VALUE);       //帮助
    pa.AddArgType("version", "version", ParsingArgs::NO_VALUE); //版本
    pa.AddArgType("g", "get", ParsingArgs::MUST_VALUE);         //获取数据
    pa.AddArgType("i", "insert", ParsingArgs::MUST_VALUE);      // 插入数据
    pa.AddArgType("n", "number", ParsingArgs::NO_VALUE);  //获取数据库中记录数
    pa.AddArgType("k", "key", ParsingArgs::MUST_VALUE);   // key
    pa.AddArgType("v", "value", ParsingArgs::MUST_VALUE); // value
    pa.SetUniqArgs("help:version");
    pa.SetMutexArgs("help:version:h");
    pa.SetDependence("h", "g|i|n");
    pa.SetDependence("g", "k");
    pa.SetDependence("i", "k&v");
    bool bExit = false;
    int iRet = pa.Parse(argc, argv, result, errPos);
    do {
        if (0 > iRet) {
            cout << "参数错误 " << iRet << " " << errPos << endl;
        } else {
            map<std::string, std::vector<std::string> >::iterator it = result.begin();
            for (; it != result.end(); ++it) {
                cout << "key=" << it->first << endl;
                for (int i = 0; i < it->second.size(); ++i) {
                    cout << "   value =" << it->second[i] << "------" << endl;
                }
            }
        }
        result.clear();
        string str, tmpPara;
        cout << ">>> ";
        getline(cin, tmpPara);
        if (0 == tmpPara.compare("exit")) {
            bExit = true;
        } else {
            iRet = pa.Parse(tmpPara, result, errPos);
        }

    } while (!bExit);
    return 0;
}
