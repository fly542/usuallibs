#!/bin/bash
################################################################################
#  Copyright (C) 2022 IQIYI All rights reserved.
#
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Tue 12 Jul 2022 02:35:27 PM CST
#  Destription: 
#           执行以下命令生成一个server
#          python -m SimpleHTTPServer 8888
#
################################################################################
cd `dirname $0`

#  是否显示执行过程
#set -x 

python -m SimpleHTTPServer 8888
