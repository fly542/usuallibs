#!/bin/bash
################################################################################
#  Copyright (C) 2023 IQIYI All rights reserved.
#
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Fri 21 Apr 2023 09:25:34 AM CST
#  Destription: 在多个机器上批量执行简单的命令
#
################################################################################
cd `dirname $0`

#  是否显示执行过程
#set -x 

cd `dirname $0`

# 要执行的机器列表,每行一个
IPS=( 
    "1.1.2.2"
    "1.1.2.3"
)

#${#array[@]}获取数组长度用于循环
function run_cmd() {
    local CMD=$1
    for(( i=0;i<${#IPS[@]};i++)) do
        ip=${IPS[i]};
        echo "${ip}"
        ssh root@"$ip" "${CMD}"
        echo ""
    done
}

run_cmd "ls -lh /etc/init.d/"
