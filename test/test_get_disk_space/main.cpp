/**
 * Copyright (C) 2015-2018 IQIYI
 * All rights reserved.
 *
 * AUTHOR(S)
 * 王海斌
 *   E-mail: wanghaibin@qiyi.com
 * 
 * VERSION
 *   Tue 26 Jun 2018 10:35:07 AM CST
 */

#include <sys/statvfs.h>
#include <iostream>
#include <stdio.h>
using namespace std;

int main(int argc, char ** argv)
{
    if(argc != 2) {
        printf("usage:\n%s disk_dir\n", argv[0]);
        return 0;
    }
    std::string path= argv[1];
    struct statvfs diskInfo;
    statvfs(path.c_str(),&diskInfo);
    int i=1024; //2的10次方，右移一次相当于除2
    printf("i=%d, i>>5=%d\n", i, i>>5);
    unsigned long long totalBlocks = diskInfo.f_bsize;
    unsigned long long totalSize = totalBlocks * diskInfo.f_blocks;
    printf("TOTAL_SIZE == %ld MB, %ldGB blocksize=%ld, blocknum=%lld\n",
            totalSize>>20, totalSize>>30, totalBlocks, diskInfo.f_blocks);

    unsigned long long freeDisk = diskInfo.f_bavail*totalBlocks;
    printf("DISK_FREE == %ld MB, %ldGB\n",freeDisk>>20, freeDisk>>30);
    return freeDisk>>20;
}
