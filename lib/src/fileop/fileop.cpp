#include "fileop.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

FileOp::FileOp() : m_fileHandle(0)
{
}

FileOp::~FileOp()
{
    Close();
}

bool FileOp::Open(const char *filePath, int oflag)
{
    if (NULL != filePath) {
        if (m_fileHandle > 0) {
            Close();
        }
        if (0 >= (m_fileHandle = open(filePath, oflag, 0666))) {
            return false;
        }
        return true;
    }
    return false;
}

void FileOp::Close()
{
    if (m_fileHandle > 0) {
        close(m_fileHandle);
        m_fileHandle = 0;
    }
}

void FileOp::WriteLine(const char *data, size_t nbyte, bool bLine)
{
    if (NULL != data && 0 < m_fileHandle) {
        write(m_fileHandle, data, nbyte);
        if (bLine) {
            char enter = '\n';
            write(m_fileHandle, &enter, 1);
        }
    }
}
void FileOp::WriteLine(const std::string &data)
{
    WriteLine(data.c_str(), data.size(), true);
}
