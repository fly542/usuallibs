/**
 * Copyright (C) 2015-2018 IQIYI
 * All rights reserved.
 *
 * AUTHOR(S)
 * Wang Haibin
 *   E-mail: wanghaibin@qiyi.com
 * 
 * VERSION
 *   Wed 18 Oct 2017 09:51:13 PM CST
 */

int CheckByteOrder()
{
    int iRet = 0;
    union {
        short s;
        char c[sizeof(short)];
    } un;
    un.s = 0x0102;
    if(sizeof(short) == 2) {
        if (un.c[0] == 1 && un.c[1] == 2) {
            iRet = 1;
        } else if (un.c[0]==2 && un.c[1]==1) {
            iRet = 2;
        }
    }
    return iRet;
}
