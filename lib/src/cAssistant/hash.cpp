#include "hash.h"
#include <string.h>

long RSHash(const std::string &str)
{
    int b = 378551;
    int a = 63689;
    long hash = 0;
    for (size_t i = 0; i < str.length(); i++) {
        hash = hash * a + str.at(i);
        a = a * b;
    }
    return hash;
}
/* End Of RS Hash Function */

long JSHash(const std::string &str)
{
    long hash = 1315423911;
    for (size_t i = 0; i < str.length(); i++) {
        hash ^= ((hash << 5) + str.at(i) + (hash >> 2));
    }
    return hash;
}
/* End Of JS Hash Function */

long PJWHash(const std::string &str)
{
    long BitsInUnsignedInt = (long)(4 * 8);
    long ThreeQuarters = (long)((BitsInUnsignedInt * 3) / 4);
    long OneEighth = (long)(BitsInUnsignedInt / 8);
    long HighBits = (long)(0xFFFFFFFF) << (BitsInUnsignedInt - OneEighth);
    long hash = 0;
    long test = 0;
    for (size_t i = 0; i < str.length(); i++) {
        hash = (hash << OneEighth) + str.at(i);
        if ((test = hash & HighBits) != 0) {
            hash = ((hash ^ (test >> ThreeQuarters)) & (~HighBits));
        }
    }
    return hash;
}
/* End Of  P. J. Weinberger Hash Function */

long ELFHash(const std::string &str)
{
    long hash = 0;
    long x = 0;
    for (size_t i = 0; i < str.length(); i++) {
        hash = (hash << 4) + str.at(i);
        if ((x = hash & 0xF0000000L) != 0) {
            hash ^= (x >> 24);
        }
        hash &= ~x;
    }
    return hash;
}
/* End Of ELF Hash Function */

long BKDRHash(const std::string &str)
{
    long seed = 131; // 31 131 1313 13131 131313 etc..
    long hash = 0;
    for (size_t i = 0; i < str.length(); i++) {
        hash = (hash * seed) + str.at(i);
    }
    return hash;
}
/* End Of BKDR Hash Function */

long SDBMHash(const std::string &str)
{
    long hash = 0;
    for (size_t i = 0; i < str.length(); i++) {
        hash = str.at(i) + (hash << 6) + (hash << 16) - hash;
    }
    return hash;
}
/* End Of SDBM Hash Function */

long DJBHash(const std::string &str)
{
    long hash = 5381;
    for (size_t i = 0; i < str.length(); i++) {
        hash = ((hash << 5) + hash) + str.at(i);
    }
    return hash;
}
/* End Of DJB Hash Function */

long DEKHash(const std::string &str)
{
    long hash = str.length();
    for (size_t i = 0; i < str.length(); i++) {
        hash = ((hash << 5) ^ (hash >> 27)) ^ str.at(i);
    }
    return hash;
}
/* End Of DEK Hash Function */

long BPHash(const std::string &str)
{
    long hash = 0;
    for (size_t i = 0; i < str.length(); i++) {
        hash = hash << 7 ^ str.at(i);
    }
    return hash;
}
/* End Of BP Hash Function */

long FNVHash(const std::string &str)
{
    long fnv_prime = 0x811C9DC5;
    long hash = 0;
    for (size_t i = 0; i < str.length(); i++) {
        hash *= fnv_prime;
        hash ^= str.at(i);
    }
    return hash;
}
/* End Of FNV Hash Function */

long APHash(const std::string &str)
{
    long hash = 0xAAAAAAAA;
    for (size_t i = 0; i < str.length(); i++) {
        if ((i & 1) == 0) {
            hash ^= ((hash << 7) ^ str.at(i) ^ (hash >> 3));
        } else {
            hash ^= (~((hash << 11) ^ str.at(i) ^ (hash >> 5)));
        }
    }
    return hash;
}
/* End Of AP Hash Function */

/* CRC Hash Function */
unsigned int CRCHash(char *str)
{
    unsigned int nleft = strlen(str);
    unsigned long long sum = 0;
    unsigned short int *w = (unsigned short int *)str;
    unsigned short int answer = 0;

    /*
     * Our algorithm is simple, using a 32 bit accumulator (sum), we add
     * sequential 16 bit words to it, and at the end, fold back all the
     * carry bits from the top 16 bits into the lower 16 bits.
     */
    while (nleft > 1) {
        sum += *w++;
        nleft -= 2;
    }
    /*
     * mop up an odd byte, if necessary
     */
    if (1 == nleft) {
        *(unsigned char *)(&answer) = *(unsigned char *)w;
        sum += answer;
    }
    /*
     * add back carry outs from top 16 bits to low 16 bits
     * add hi 16 to low 16
     */
    sum = (sum >> 16) + (sum & 0xFFFF);
    /* add carry */
    sum += (sum >> 16);
    /* truncate to 16 bits */
    answer = ~sum;

    return (answer & 0xFFFFFFFF);
}
