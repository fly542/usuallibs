/**
 * 
 * @param 
 * @param 
 * @return -1 监听失败 >0失败
 */
int udp_server(const char * host, const char * serv, socketlen_t *addrlenp)
{
    int sockfd=-1, n=0;
    struct addrinfo hints, *res, *ressave;
    bzero(&hints, sizeof(struct addrinfo));
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;
    
    if(0!=(n=getaddrinfo(host, serv, &hints, &res))) {
        return sockfd;
    }
    ressave = res;

    do {
        sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if(sockfd<0) {
            continue;
        }

        if(0==bind(sockfd, res->ai_addr, res->ai_addrlen)) {
            break;
        }
        close(sockfd);
        sockfd = -1;
    }while(NULL!=(res=res->ai_next));

    if(sockfd>=0) {
        if ( addrlenp ) {
            *addrlenp = res->ai_addrlen;
        }
    }

    freeaddrinfo(ressave);
    return sockfd;
}
