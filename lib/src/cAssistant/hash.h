/* pur @ 各类hash函数汇总
 * date @ 2014.11.25
 * author @ wanghaibin
 */

#ifndef _HASH_H
#define _HASH_H

#include <string>
#ifdef __cplusplus
extern "C" {
#endif

/* RS Hash Function */
long RSHash(const std::string &str);

/* JS Hash Function */
long JSHash(const std::string &str);

/* P. J. Weinberger Hash Function */
long PJWHash(const std::string &str);

/* ELF Hash Function */
long ELFHash(const std::string &str);

/* BKDR Hash Function */
long BKDRHash(const std::string &str);

/* SDBM Hash Function */
long SDBMHash(const std::string &str);

/* DJB Hash Function */
long DJBHash(const std::string &str);

/* DEK Hash Function */
long DEKHash(const std::string &str);

/* BP Hash Function */
long BPHash(const std::string &str);

/* FNV Hash Function */
long FNVHash(const std::string &str);

/* AP Hash Function */
long APHash(const std::string &str);

/* CRC Hash Function */
unsigned int CRCHash(char *str);

#ifdef __cplusplus
}
#endif

#endif
