/* pur @ 定义共用宏定义文件
 * date @ 2013.10.25
 * author @ haibin.wang
*/

#ifndef DEFCOM_H
#define DEFCOM_H

#define MAXBACKLOG 1000 // listen  最大排队数目

#define RECV_BUF_SIZE 1024 * 64 //接收缓冲区大小
#define SEND_BUF_SIZE 1024 * 16 //发送缓冲区大小

#endif