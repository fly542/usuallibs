/**
 * Copyright (C) 2015-2018 IQIYI
 * All rights reserved.
 *
 * Author     : 王海斌
 * E-mail     : wanghaibin@qiyi.com
 * Version    :
 * Date       : Mon 07 Jan 2019 05:35:29 PM CST
 * Destription: aes 加密测试程序
 *
 */

#include "aes_encryptor.h"

#include <iostream>

using namespace std;


int main(int , char *[])
{
    string teststr="abc_efghijZlmnop@rs*lmn1234-6789:wq";

    unsigned char keys[] = { 244, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 123, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
    AesEncryptor encry(keys);
    std::string enstr = encry.EncryptString(teststr);
    std::string destr = encry.DecryptString(enstr);
    cout << "ori=" << teststr << endl;
    cout << "enstr=" << enstr << endl;
    cout << "destr=" << destr << endl;
    return 0;
}
