#include "singleconfig.h"
#include "configureinfo.h"
#include <sstream>

SingleConfig *SingleConfig::m_instance = NULL;
ConfigureInfo *SingleConfig::m_conf = NULL;

SingleConfig::~SingleConfig()
{
}

void SingleConfig::Release()
{
    if (m_instance) {
        delete m_instance;
        m_instance = NULL;
    }

    if (m_conf) {
        delete m_conf;
        m_conf = NULL;
    }
}

SingleConfig *SingleConfig::Instance()
{
    if (NULL == m_instance) {
        m_instance = new SingleConfig();
    }
    if (NULL == m_conf) {
        m_conf = new ConfigureInfo();
    }
    return m_instance;
}

bool SingleConfig::SetConfigPath(const char *dir, bool bChild, std::string &err)
{
    Instance();
    return m_conf->ReadConfigue(dir, bChild, err);
}

bool SingleConfig::SetConfigPath(const char *configFile, std::string &err)
{
    Instance();
    return m_conf->ReadConfigue(configFile, err);
}

bool SingleConfig::SetDefaultValue(const char *filename, const char *key, const char *defValue)
{
    if (NULL != m_conf) {
        return m_conf->SetDefaultValue(filename, key, defValue);
    } else {
        return false;
    }
}

bool SingleConfig::SetDefaultValue(const char *filename, const char *key, const double &defValue)
{
    if (NULL != m_conf) {
        return m_conf->SetDefaultValue(filename, key, defValue);
    } else {
        return false;
    }
}

bool SingleConfig::SetDefaultValue(const char *filename, const char *key, const long long &defValue)
{
    if (NULL != m_conf) {
        return m_conf->SetDefaultValue(filename, key, defValue);
    } else {
        return false;
    }
}

bool SingleConfig::SetDefaultValue(const char *filename, const char *key, const int &defValue)
{
    if (NULL != m_conf) {
        return m_conf->SetDefaultValue(filename, key, defValue);
    } else {
        return false;
    }

    if (NULL != m_conf) {
        std::stringstream ss;
        std::string str;
        ss << defValue;
        ss >> str;
        return m_conf->SetDefaultValue(filename, key, str.c_str());
    } else {
        return false;
    }
}

std::string SingleConfig::GetStrValue(const char *modulename, const char *key)
{
    if (NULL != m_conf) {
        return m_conf->GetStrValue(modulename, key);
    } else {
        return std::string("");
    }
}

int SingleConfig::GetIntValue(const char *modulename, const char *key)
{
    if (NULL != m_conf) {
        return m_conf->GetIntValue(modulename, key);
    } else {
        return -65536;
    }

    std::string value = GetStrValue(modulename, key);
    if (value.empty()) {
        return -65536;
    } else {
        std::istringstream iss(value);
        int num;
        iss >> num;
        return num;
    }
}

long long SingleConfig::GetLongValue(const char *modulename, const char *key)
{
    if (NULL != m_conf) {
        return m_conf->GetLongValue(modulename, key);
    } else {
        return 0;
    }
}

double SingleConfig::GetDoubleValue(const char *modulename, const char *key)
{
    if (NULL != m_conf) {
        return m_conf->GetDoubleValue(modulename, key);
    } else {
        return 0.0f;
    }
}

bool SingleConfig::GetBoolValue(const char *modulename, const char *key)
{
    int iNum = GetIntValue(modulename, key);
    return 1 == iNum ? true : false;
}

bool SingleConfig::ReloadModule(const char *modulename)
{
    if (NULL != m_conf) {
        return m_conf->ReloadOne(modulename);
    } else {
        return false;
    }
}

void SingleConfig::AddAutoReloadFile(const char *filepath)
{
    if (NULL != m_conf) {
        m_conf->AddAutoReloadFile(filepath);
    }
}

void SingleConfig::StartAutoReload(const int &checkInter)
{
    if (NULL != m_conf) {
        m_conf->StartAutoReload(checkInter);
    }
}

std::string SingleConfig::GetFileName(const char *filePath)
{
    std::string strPath(filePath);
    if (strPath.empty()) {
        return strPath;
    } else {
        size_t pos = strPath.rfind('/');
        if (pos != std::string::npos) {
            return strPath.substr(pos + 1);
        } else {
            return strPath;
        }
    }
}
