/**
 * Copyright (c) 2015-2018 iqiyi
 * All rights reserved.
 *
 * Author     : Wang Haibin
 * E-mail     : wanghaibin@qiyi.com
 * Version    :
 * Date       : Fri 20 Nov 2020 11:13:46 AM CST
 * Destription: 基于引用计数的测试任务对象
 */
#pragma once
#include "iqdb_reference.h"
#include "iqdb_reference_base.h"

namespace IQDB {

class iqdb_task : public iqdb_reference_table
{
public:
    typedef iqdb_reference<iqdb_task> pointer; //引用

    iqdb_task (int );
    virtual ~iqdb_task ();

    void test_fun();
private:
    int a_;
    /* data */
};

// 创建基于引用计数的iqdb_task 对象
iqdb_task::pointer create_iqdb_task();
} /* IQDB  */
