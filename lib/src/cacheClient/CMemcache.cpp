#include "CMemcache.h"

#include <iostream>
using namespace std;
CMemcache::CMemcache() : m_conn(NULL), m_timeout(EXPIRATION)
{
}

CMemcache::CMemcache(unsigned int time) : m_conn(NULL), m_timeout(time)
{
}

CMemcache::~CMemcache()
{
    Close();
}

bool CMemcache::IsVaild()
{
    return (NULL == m_conn) ? false : true;
}

bool CMemcache::Connect(string ip, unsigned int port)
{
    memcached_server_st *server = NULL;
    memcached_return result;
    server = memcached_server_list_append(server, (char *)ip.c_str(), port, &result);
    if (MEMCACHED_SUCCESS != result) {
        return false;
    }
    m_conn = memcached_create(NULL);

    if (MEMCACHED_SUCCESS != memcached_server_push(m_conn, server)) {
        memcached_server_list_free(server);
        Close();
        return false;
    }

    if (MEMCACHED_SUCCESS != memcached_behavior_set(m_conn, MEMCACHED_BEHAVIOR_NO_BLOCK, 1)) {
        memcached_server_list_free(server);
        Close();
        return false;
    }
    memcached_server_list_free(server);
    return true;
}

void CMemcache::Close()
{
    memcached_free(m_conn);
    m_conn = NULL;
}

string CMemcache::Get(string key)
{
    char *buffer = NULL;
    string value = "";

    memcached_return result;
    size_t value_length;
    uint32_t flag = 32;

    buffer = memcached_get(m_conn, key.c_str(), key.size(), &value_length, &flag, &result);

    if (MEMCACHED_SUCCESS == result) {
        value = string(buffer);
        free(buffer);
    }
    return value;
}

bool CMemcache::Add(string key, string value)
{
    memcached_return result;
    result = memcached_add(m_conn, key.c_str(), key.size(), value.c_str(), value.size(),
                           (time_t)m_timeout, (uint32_t)0);
    return (MEMCACHED_SUCCESS == result) ? true : false;
}

bool CMemcache::Replace(string key, string value)
{
    memcached_return result;

    result = memcached_replace(m_conn, key.c_str(), key.size(), value.c_str(),
                               value.size(), (time_t)m_timeout, (uint32_t)0);
    return (MEMCACHED_SUCCESS == result) ? true : false;
}

bool CMemcache::Set(string key, string value)
{
    memcached_return result;

    result = memcached_set(m_conn, key.c_str(), key.size(), value.c_str(), value.size(),
                           (time_t)m_timeout, (uint32_t)0);
    return (MEMCACHED_SUCCESS == result) ? true : false;
}

bool CMemcache::Delete(string key)
{
    memcached_return result;

    result = memcached_delete(m_conn, key.c_str(), key.size(), EXPIRATION);
    return (MEMCACHED_SUCCESS == result) ? true : false;
}
