num=1000000
logfile="log/1.log"
valgLog="log/val.log"
echo "" > ${logfile}
for ((i=0; i<${num}; ++i))
do
    echo "test seq${i}" >> ${logfile}
    echo "" >> ${logfile}

    ./testPool 50 50 10 0 1 >>${logfile} 
    #valgrind --tool=memcheck --leak-check=full --track-origins=yes --show-reachable=yes   --log-file=log/valgrind_log ./test 100 100 3 0 >> ${logfile}
done
