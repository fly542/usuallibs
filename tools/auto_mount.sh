#!/bin/bash
################################################################################
#  Copyright (C) 2022 IQIYI All rights reserved.
#
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Tue 07 Jun 2022 09:04:38 AM CST
#  Destription: 自动挂载本机所有磁盘脚本
#
################################################################################
cd `dirname $0`

#  是否显示执行过程
#set -x 

#!/bin/bash

[ ! -d /mnt/read ] && mkdir /mnt/read

for i in $(/sbin/fdisk -l 2>/dev/null | grep -e "Disk /dev/sd" |cut -d" " -f2|sed "s/://g")
do
    mount -t xfs $i /mnt/read && dir=$(cat /mnt/read/.info)
    umount -f $i
    mount -t xfs $i $dir
done

