
#include <stdio.h>

#include "datetime.h"

CDateTime::CDateTime()
{
    m_tTime = time(NULL);
    localtime_r(&m_tTime, &m_stTime);
}

CDateTime::CDateTime(time_t tTime)
{
    m_tTime = tTime;
    localtime_r(&m_tTime, &m_stTime);
}

CDateTime::CDateTime(const struct tm &stTime)
{
    m_stTime = stTime;
    m_tTime = mktime(&m_stTime);
}

CDateTime::CDateTime(int iYear, int iMonth, int iDay, int iHour, int iMinute, int iSecond)
{
    m_stTime.tm_year = iYear - 1900;
    m_stTime.tm_mon = iMonth - 1;
    m_stTime.tm_mday = iDay;
    m_stTime.tm_hour = iHour;
    m_stTime.tm_min = iMinute;
    m_stTime.tm_sec = iSecond;

    m_tTime = mktime(&m_stTime);
}

string CDateTime::ShortDateTime()
{
    char sDateTime[15];
    sprintf(sDateTime, "%04d%02d%02d%02d%02d%02d", Year(), Month(), Day(), Hour(),
            Minute(), Second());
    return string(sDateTime);
}

string CDateTime::LongDateTime(char chSep1, char chSep2)
{
    char sDateTime[100] = { 0 };
    sprintf(sDateTime, "%04d%c%02d%c%02d %02d%c%02d%c%02d", Year(), chSep1, Month(),
            chSep1, Day(), Hour(), chSep2, Minute(), chSep2, Second());
    return string(sDateTime);
}

string CDateTime::ShortDate()
{
    char sDate[9];
    sprintf(sDate, "%04d%02d%02d", Year(), Month(), Day());
    return string(sDate);
}

string CDateTime::LongDate(char chSep)
{
    char sDate[11];
    sprintf(sDate, "%04d%c%02d%c%02d", Year(), chSep, Month(), chSep, Day());
    return string(sDate);
}
