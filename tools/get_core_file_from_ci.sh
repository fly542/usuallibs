#!/bin/bash
################################################################################
#  Copyright (C) 2022 IQIYI All rights reserved.
#
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Fri 25 Nov 2022 10:54:54 AM CST
#  Destription: 当ci执行过程中程序崩溃时获取进程和对应的core文件
#
################################################################################
cd `dirname $0`

#  是否显示执行过程
#set -x 

# 检测当前目录下的 core 文件, 有的话使用gdb进行core信息输出
# 传递第一个参数为可执行程序文件名
function print_core() {
    local proc_name=$1
    for core_file in `ls`
    do
        # -d表示这个是 文件夹
        if [ -f ${core_file} ]
        then
            # 判断是core.开头的文件
            if echo ${core_file} | grep -q -E '^core.'
            then
                # 输出文件的地址
                echo "******************find core file ${core_file} ${proc_name}*************************"
                gdb -q --batch --ex "set height 0" -ex "thread apply all bt full"  ${proc_name} ${core_file}
                echo "***********************************************************************"
                local tar_file="${proc_name}_${core_file}.tar.gz"
                tar zcvf ${tar_file} ${core_file} ${proc_name}
                curl -v -T ${tar_file} http://10.67.9.10:10081/whbupload/
            fi
        fi
    done
}

./test --all_test --verbose
RETVAL=$?

# 返回值非0 说明异常
if [ $RETVAL != 0 ]
then
        print_core test
fi
exit ${RETVAL}

