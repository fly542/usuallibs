/**
 * Copyright (C) 2015-2018 IQIYI
 * All rights reserved.
 *
 * Author     : 王海斌
 * E-mail     : wanghaibin@qiyi.com
 * Version    :
 * Date       : Sat 13 Apr 2019 04:04:19 PM CST
 * Destription: 测试时间转换相关函数
 */


#include <stdio.h>
#include <time.h>
#include <stdint.h>

int main(void)
{
    time_t result;

    result = time(NULL);
    printf("%s%ju secs since the Epoch\n",
            asctime(localtime(&result)),
            (uintmax_t)result);
    return(0);
}

