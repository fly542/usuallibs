#!/bin/bash
################################################################################
#  Copyright (C) 2015-2018 IQIYI All rights reserved.
# 
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Fri 30 Nov 2018 11:12:21 AM CST
#  Destription: 创建文件映射挂载点
#
################################################################################
cd `dirname $0`

#映射文件
diskFile=$1
#挂载路径
mountDir=$2
#映射空间大小 单位MB
fileSize=$3

if [ -z "${diskFile}" -o -z "${mountDir}" ]; then
    echo "useage: $0 mapfile mountdir size(MB)"
    echo "eg: $0 /root/data.img /dir1 10240"
    exit 0
fi


# 1、 安装mkfs.ext4或xfs系统格式化工具
yum install -y e4fsprogs
#yum -y install xfsprogs
# 2、查询当前可用设备
curDevice=`losetup -f`
if [ ! -d ${mountDir} ] ; then
    mkdir -p ${mountDir}
fi

if [ ! -b ${curDevice} ] ; then
    seq=`echo ${curDevice} |awk -F'loop' '{print $2}' `
    mknod -m 0660 ${curDevice} b 7 ${seq}
    echo "not exist ${curDevice}, seq=${seq}"
else 
    echo "exist ${curDevice}"
fi
# 3、创建镜像文件
dd if=/dev/zero of=${diskFile} bs=1M count=${fileSize}
# 4、使用 losetup将磁盘镜像文件虚拟成块设备
losetup ${curDevice} ${diskFile}
# 5、在块设备上创建文件系统
mkfs.ext4 ${curDevice}
# 6、mount挂载文件系统, 加上sync，确保文件可以及时地写入到映像中
mount ${diskFile} ${mountDir} -o sync
# 7、卸载回环设置
losetup -d ${curDevice}

