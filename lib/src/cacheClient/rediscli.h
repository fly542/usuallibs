#ifndef __REDISCLI_H__
#define __REDISCLI_H__

/* purpose @ redis 客户端封装
 * date    @ 2015-11-15
 * author  @ haibin.wang
 */
#include <string>

class redisContext;

class RedisClient {
    public:
    explicit RedisClient();
    virtual ~RedisClient();

    bool Connect(const char *ip, int port, int timeout);
    bool Reconnect();

    public:
    // key

    /* pur @ 设置过期时间，
     * para @ key 要过期的key
     * para @ seconds 从当前时间后推的秒数
     * return @ true 成功 false 失败
    */
    bool Expire(const char *key, int seconds);

    /* pur @ 删除一个指定的key,不存在也认为删除成功
     * para @ key 要删除的key
     * return @ true 成功 false 失败
    */
    bool Del(const char *key);

    public:
    // string

    /* pur @ 添加key-value到redis中
     * para @ key 要添加的key
     * para @ value 对应的value
     * para @ timeout >0则设置超时时间，<=0则永不超时，单位(s)
     * return @ true 成功，false 失败
    */
    bool Set(const char *key, const char *value, int timeout = 0);

    /* pur @ 添加key-value到redis中
     * para @ key 要添加的key
     * para @ value 对应的value
     * para @ timeout >0则设置超时时间，<=0则永不超时，单位(s)
     * return @ true 成功，false 失败
    */
    bool Set(const char *key, int value, int timeout = 0);

    /* pur @ 根据key获取对应的value值
     * para @ key
     * para @ value
     * return @ true 成功 false 获取失败
    */
    bool Get(const char *key, std::string &value);

    public:
    // SortedSet

    bool ZAdd(const char *key, long long score, const char *value);
    bool ZFirstBiggerScore(const char *key, long long score, std::string &value);
    bool ZScore(const char *key, const char *member, std::string &score);
    bool ZRemove(const char *key, const char *member);

    public:
    /* pur @ 获取最后一次操作错误的信息
     * return @ 返回错误字符串，无错误则返回NULL
    */
    const char *GetErrorInfo();

    /* pur @ 获取服务端信息
     * return @ 返回格式为 ip:port:timeout
    */
    std::string GetServerInfo();

    private:
    bool Connect();
    void DisConnect();

    private:
    redisContext *m_redis;
    std::string m_ip;
    std::string m_err; //最后一次错误的信息
    int m_port;
    int m_timeout;
    bool m_bReconn; //是否需要重连(true 需要，false 不需要)
};

#endif // __REDISCLI_H__
