/**
 * Copyright (c) 2015-2018 iqiyi
 * All rights reserved.
 *
 * Author     : Wang Haibin
 * E-mail     : wanghaibin@qiyi.com
 * Version    :
 * Date       : Fri 20 Nov 2020 01:49:59 PM CST
 * Destription: 引用计数基类
 */
#pragma once

namespace IQDB {

class iqdb_reference_base
{
public:
    iqdb_reference_base ();
    virtual ~iqdb_reference_base ();

public:
    /**
     * 增加引用计数
     */
    void reference() const;

    /**
     * 减少引用计数
     * @return 返回减少计数后的计数值
     */
    int dereference() const;
private:
    mutable unsigned int self_ref_count_;       //引用计数个数统计
};

#define iqdb_reference_table virtual iqdb_reference_base

} /* IQDB  */
