
int udp_client(const char * host, const char * serv,
        struct sockaddr_in **saptr, socklen_t * lenp)
{
    int sockfd=-1, n;
    struct addrinfo hints, *res, *ressave;
    bzero(&hints, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    if(0!=(n=getaddrinfo(host, serv, &hints, &res))) {
        return sockfd;
    }

    ressave = res;

    do {
        sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if(sockfd >=0) {
            break;
        }
    }while(NULL!=(res=res->ai_next));

    if(NULL!=res) {
        *saptr = malloc(res->ai_addrlen);
        memcpy(*saptr, res->ai_addr, res->ai_addrlen);
        *lenp = res->ai_addrlen;
    }
    freeaddrinfo(ressave);
    return sockfd;
}
