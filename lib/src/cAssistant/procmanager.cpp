
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "comfun.h"
#include "procmanager.h"

/* pur @使当前进程变为守护进程
 */
void MakeDaemon(void)
{
    pid_t pid = 0;
    if (0 < (pid = fork())) {
        exit(0); //是父进程，结束父进程
    } else if (pid < 0) {
        exit(1); // fork失败，退出
    }

    //是第一子进程，后台继续执行
    setsid(); //第一子进程成为新的会话组长和进程组长
    //与控制终端分离
    if (0 < (pid = fork())) {
        exit(0); //是第一子进程，结束第一子进程
    } else if (pid < 0) {
        exit(1); // fork失败，退出
    }
    // chdir("/tmp"); //改变工作目录
    // umask(0);//重设文件掩码
}

/* pur @ 启动子进程
 * para @ pid //子进程id
 * para @ command 要启动的进程的全路径和参数列表串
 * return @
 */
void StartProc(int *pid, const char *command)
{
    char *argv[50] = { 0 };
    std::vector<std::string> vecComm;
    Split(command, " ", vecComm);
    for (size_t i = 0; i < vecComm.size(); ++i) {
        argv[i] = strdup(vecComm[i].c_str());
        // printf("para %d=%s\n",i, argv[i]);
    }

    int iPid = -1;
    iPid = fork();
    if (iPid == 0) { //子进程
        *pid = getpid();
        if (-1 == execvp(argv[0], argv)) {
            //	printf("start proc %s failed, err->%s\n", argv[0], strerror(errno));
            exit(0);
        } else {
            //	printf("start proc %s succeed\n", argv[0]);
        }

    } else if (iPid > 0) { //父进程
        *pid = iPid;
    }

    for (size_t j = 0; j < vecComm.size(); ++j) {
        if (NULL != argv[j]) {
            free(argv[j]);
            argv[j] = NULL;
        }
    }
}
