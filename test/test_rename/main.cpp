/**
 * Copyright (C) 2015-2018 IQIYI
 * All rights reserved.
 *
 * Author     : 王海斌
 * E-mail     : wanghaibin@qiyi.com
 * Version    :
 * Date       : Wed 16 Jan 2019 10:54:08 AM CST
 * Destription:  测试rename 文件情况
 *
 */

#include <stdio.h>

int main(int , char *[])
{
    int iRet = rename("./a.txt", "./b.txt");
    printf("iRet =%d\r\n", iRet);
    return 0;
}
