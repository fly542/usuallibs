/**
 * Copyright (C) 2015-2018 IQIYI
 * All rights reserved.
 *
 * Author     : 王海斌
 * E-mail     : wanghaibin@qiyi.com
 * Version    :
 * Date       : Fri 20 Nov 2020 02:04:47 PM CST
 * Destription:
 */

#include "stdafx.h"
#include "iqdb_task.h"
#include "iqdb_reference.h"

int main(int , char *[])
{
    IQDB::iqdb_task::pointer tmp = IQDB::create_iqdb_task(); //  使用方式1
    //IQDB::iqdb_task::pointer tmp(new IQDB::iqdb_task(100)); // 使用方法2
    //iqdb_ref<iqdb_task> tmp(new iqdb_task(100)); // 使用方式3
    {
        printf("增加一次引用计数 !tmp=%d\n", !tmp);
        iqdb_reference<IQDB::iqdb_task> tmp2 = tmp; // 赋值构造则增加引用计数
        tmp->test_fun(); //按照指针方式进行使用，因为iqdb_ref重载了->
        IQDB::iqdb_task * task = tmp.get_raw(); //获取原始指针
        printf("origin task=%p\n", task);
        //IQDB::iqdb_task::pointer xx = new IQDB::iqdb_task();//IQDB::create_iqdb_task();
    }
    printf("tmp2 销毁时将 释放一次引用计数\n");
    return 0;
}
