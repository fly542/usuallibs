#!/bin/sh

#valgrind --tool=memcheck  --leak-check=full --show-reachable=yes -v --track-origins=yes  --log-file=val.log $1
#valgrind --tool=memcheck  --leak-check=full --gen-suppressions=all   -v  --log-file=val.log $1
valgrind   --tool=memcheck  --leak-check=full --track-origins=yes --gen-suppressions=yes  --trace-children=yes --show-possibly-lost=yes  -v  --log-file=val.log $1
#valgrind   --tool=callgrind   $1
