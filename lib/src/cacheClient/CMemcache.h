#ifndef _CMEMCACHE_H_
#define _CMEMCACHE_H_

#include <libmemcached/memcached.h>
#include <string>
using std::string;

#ifndef EXPIRATION
#define EXPIRATION 0x0
#endif

class CMemcache {
    public:
    CMemcache();
    CMemcache(unsigned int time);
    ~CMemcache();
    bool Connect(string ip, unsigned int port);
    void Close();
    bool IsVaild();

    string Get(string key);
    bool Add(string key, string value);
    bool Replace(string key, string value);
    bool Set(string key, string value);
    bool Delete(string key);

    private:
    memcached_st *m_conn;
    unsigned int m_timeout;
};

#endif /*_mCMEMCACHE_H_*/
