/* purpose @ 常用函数和封装类,
 *          目标只要两个文件comfun.h和comfun.cpp就可以实现一些常用的函数
 * date    @ 2014.04.01
 * author  @ haibin.wang
 */

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// MapData用户文件映射结构体
struct MapData {
    void *m_data;
    int m_length;
};

/* pur @ 释放文件映射资源
 * para @ mpData 通过MMAPFile获取到的映射资源
 */
void UnMMAPFile(MapData &mpData);

/* pur @ 获取文件映射资源，通过 UnMMAPFile 释放相关的资源
 * para @ openFd 已打开的文件句柄
 * para @ fileLen 要映射的资源长度
 * return @ true映射成功，false 失败
 */
bool MMAPFile(int openFd, size_t fileLen, MapData &mpData);

/* pur @ 获取目录下所有常规文件的全路径（路径/文件名）
 * para @ dirPath 要查询的目录
 * para @ files 查询成功返回查询到的所有文件的全路径
 * return @ true 查询成功、false查询失败
*/
bool GetDirFiles(const char *dirPath, std::vector<std::string> &files);

/* pur @ 根据文件路径获取文件名
 * para @ filepath 文件的路径包含文件名（可不包含/，代表当前路径下文件）
 * return @ 非空返回文件名，空则说明filepath所指的文件不存在
*/
std::string GetFileName(const char *filepath);

/* pur @ 格式化路径，将路径格式化为标准的linux路径/xx/xxx
 *		会将路径中的\转为/并将多个连续的//修改为1个
 * para @ path 要格式化的路径
 * return @ 返回格式化后的字符串
 */
std::string FormatPath(const char *path);

/* pur @ 判定给定的路径是否是一个文件夹
 * para @ path 要判定的文件路径
 * return @ true 是文件夹，false 不是
*/
bool IsDir(const char *path);

/* pur @ 数字转字符串函数,精度只能到3位小数
 * para @ num 需要被转的数字
 * return @  返回转换后的字符串
*/
template <class Type> std::string NumberToString(const Type &num)
{
    std::stringstream ss;
    // ss.precision(18);
    std::string str;
    ss << num;
    ss >> str;
    return str;
}

/* pur @ 字符串分隔函数
 * para @ str 要分隔的字符串
 * para @ pattern 分隔符，不能按照多个分隔符分隔
 * para @ 返回分隔后的字符串列表，顺序按照str的先后顺序存放
*/
void Split(const std::string &str, const std::string &pattern, std::vector<std::string> &result);

/* pur @ 字符串转数字
 * para @ str 被转为数字的字符串
 * return @  返回转换后的数字
*/
template <class Type> Type StringToNumber(const std::string &str)
{
    std::istringstream iss(str);
    Type num;
    iss >> num;
    return num;
}

/* pur @ 去掉字符串前后的空白字符，包括空格和tab符合
 * para @ line 要被删除空白字符的字符串
*/
void TrimLine(std::string &line);

/* pur @ 检测文件是否存在
 * para @ 要检测的文件
 * return @ true 存在，false 不存在
 */
bool IsExist(const char *filepath);

/* pur @ 获取文件大小
 * para @ filepath 要获取的文件路径
 * return @ 返回获取到的文件大小
 */
size_t FileSize(const char *path);

/* pur @ 创建目录
 * para @ path 要创建的目录
 * return @ true 创建成功，false失败
 */
bool Mkdir(const char *path);

/**
 * 创建多级目录
 * @param path 要创建的完整目录，不包含文件名
 *              /a/b/c 同时创建a b c 三级目录
 * @param mode 目录权限， 0755 ，0744 等
 * @return true 成功，false失败
 */
bool Mkdirs(const char *path, int mode);

/******************************************************************************
 *
 * 以下函数为网络常用函数
 *
 * ****************************************************************************/

/* pur @ 检查addr格式的正确性，格式为ip:port,ip1:port1,ip2:port2
 * para @ addr ip:port地址列表
 * return @ true符合ip格式要求，false, 不符合格式要求
*/
bool CheckSockAddr(const char *addr);

/* pur @ 获取本机除127.0.0.1外的ip地址
 * para @ ips 存放获取到的ip
 * para @ bIpV6 是否获取ipv6的地址
 * return @
 */
void GetLocalIps(std::vector<std::string> &ips, bool bIpV6);

/**
* 检测ip是否是局域网ip
* @param ip 要检测的ip地址
* @return true 是局域网ip，false 不是局域网ip
*/
bool CheckLocalIp(const std::string& ip);
