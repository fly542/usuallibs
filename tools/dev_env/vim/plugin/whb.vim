"加入.h .hpp的头注释
func WHB_SetComment_C()
    call setline(1,"/**")
    call append(line(".")   , "* Copyright (C) ".strftime("%Y")."")
    call append(line(".")+1 , "* All rights reserved.")
    call append(line(".")+2 , "*") 
    call append(line(".")+3 , "* Author     : Wang Haibin")
    call append(line(".")+4 , "* E-mail     : wanghaibin@qiyi.com")
    call append(line(".")+5 , "* Version    :")
    call append(line(".")+6 , "* Date       : ".strftime("%c")."")
    call append(line(".")+7 , "* Destription:") 
    call append(line(".")+8 , "*/") 
endfunc

"加入sh注释
func WHB_SetComment_SH()
    call setline(1, "#!/bin/bash")
    call append(line("."), "################################################################################")
    call append(line(".")+1, "#  Copyright (C) ".strftime("%Y")." IQIYI All rights reserved.")
    call append(line(".")+2, "#") 
    call append(line(".")+3, "#  Author     : 王海斌")
    call append(line(".")+4, "#  E-mail     : wanghaibin@qiyi.com")
    call append(line(".")+5, "#  Version    :")
    call append(line(".")+6, "#  Date       : ".strftime("%c")."")
    call append(line(".")+7, "#  Destription: ")
    call append(line(".")+8, "#")
    call append(line(".")+9, "################################################################################")
    call append(line(".")+10, "cd `dirname $0`")
    call append(line(".")+11, "")
    call append(line(".")+12, "#  是否显示执行过程")
    call append(line(".")+13, "#set -x ")
    call append(line(".")+14, "")
endfunc

"定义函数SetTitle，自动插入文件头 
func WHB_SetTitle_C()
    if expand("%:e") == 'hpp' 
        call WHB_SetComment_C()
        call append(line(".")+9 , "")
        call append(line(".")+10, "#ifndef _".toupper(expand("%:t:r"))."_H_")
        call append(line(".")+11, "#define _".toupper(expand("%:t:r"))."_H_")
        call append(line(".")+12, "#ifdef __cplusplus")
        call append(line(".")+13, "extern \"C\"")
        call append(line(".")+14, "{")
        call append(line(".")+15, "#endif")
        call append(line(".")+16, "")
        call append(line(".")+17, "#ifdef __cplusplus")
        call append(line(".")+18, "}")
        call append(line(".")+19, "#endif")
        call append(line(".")+20, "#endif //".toupper(expand("%:t:r"))."_H_")
    elseif expand("%:e") == 'sh'
        call WHB_SetComment_SH()
    elseif expand("%:e") == 'h'
        call WHB_SetComment_C()
        call append(line(".")+9, "#pragma once")
        call append(line(".")+10, "")
        "call append(line(".")+10, "#ifndef _".toupper(expand("%:t:r"))."_H_")
        "call append(line(".")+11, "#define _".toupper(expand("%:t:r"))."_H_")
        "call append(line(".")+12, "")
        "call append(line(".")+13, "#endif //".toupper(expand("%:t:r"))."_H_")
    elseif &filetype == 'c'
        call setline(1,"#include \"".expand("%:t:r").".h\"")
    elseif &filetype == 'cpp'
        call setline(1,"#include \"".expand("%:t:r").".h\"")
    endif
endfunc

" 解决UltiSnips和YCM的tab键冲突加入的重定义函数 !代表重定义一个已经存在的函数
function! g:UltiSnips_Complete()
    call UltiSnips#ExpandSnippet()
    if g:ulti_expand_res == 0
        if pumvisible()
            return "\<C-n>"
        else
            call UltiSnips#JumpForwards()
            if g:ulti_jump_forwards_res == 0
                return "\<TAB>"
            endif
        endif
    endif
    return ""
endfunction

" 解决UltiSnips和YCM的tab键冲突加入的重定义函数 !代表重定义一个已经存在的函数
function! g:UltiSnips_Reverse()
    call UltiSnips#JumpBackwards()
    if g:ulti_jump_backwards_res == 0
        return "\<C-P>"
    endif
    return ""
endfunction

if !exists("g:UltiSnipsJumpForwardTrigger")
    let g:UltiSnipsJumpForwardTrigger = "<tab>"
endif
if !exists("g:UltiSnipsJumpBackwardTrigger")
    let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
endif

" 解决UltiSnips和YCM的tab键冲突加入的重定义函数 !代表重定义一个已经存在的函数
" 为了上面的函数创建一个自动BufEnter
au InsertEnter * exec "inoremap <silent> " . g:UltiSnipsExpandTrigger     . " <C-R>=g:UltiSnips_Complete()<cr>"
au InsertEnter * exec "inoremap <silent> " .     g:UltiSnipsJumpBackwardTrigger . " <C-R>=g:UltiSnips_Reverse()<cr>"
