#!/bin/bash
################################################################################
#  Copyright (C) 2015-2018 IQIYI All rights reserved.
#
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : 2021-06-01
#  Destription: 读取目录下的文件
#
################################################################################
cd `dirname $0`

function read_dir(){
    for file in `ls $1` #注意此处这是两个反引号，表示运行系统命令
    do
        # if [ -d $1"/"$file ] #注意此处之间一定要加上空格，否则会报错
        # then
        #     read_dir $1"/"$file
        # else
        #     echo $1"/"$file #在此处处理文件即可
        # fi
        if [ -f $1"/"$file ]
        then
            echo $1"/"$file #在此处处理文件即可
        fi
    done
}

read_dir $1
