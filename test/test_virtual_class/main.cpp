/**
 * Copyright (C) 2015-2018 IQIYI
 * All rights reserved.
 *
 * Author     : 王海斌
 * E-mail     : wanghaibin@qiyi.com
 * Version    :
 * Date       : Tue 29 Jan 2019 10:20:31 AM CST
 * Destription:  测试基础纯虚类函数
            关键验证结果可以继续使用纯虚函数继承基类的纯虚函数
 *
 */
#include <iostream>

using namespace std;

class Base
{
public:
    Base (){data = 0;}
    virtual ~Base (){}

    void testCall()
    {
        vfun();
    }
protected:
    virtual void vfun() = 0;

    int data;
};

class Child1 : protected Base
{
public:
    Child1 () {}
    virtual ~Child1 () {}

    void testCall2() {
        testCall();
    }
protected: //关键点是可以继续使用纯虚函数继承基类的纯虚函数
    virtual void vfun()=0;
};


class Child2 : public Child1
{
public:
    Child2 () {}
    virtual ~Child2 () {}

    void SetData(int v) {
        data = v;
    }
protected:
    virtual void vfun(){ cout << "func2=" << data << endl;}
private:
    /* data */
};

class UseCl
{
public:
    UseCl (){}
    virtual ~UseCl (){}

    void func(Child1 * base)
    {
        if(base) {
            base->testCall2();
        }
    }
private:
    Base * bl;
};

int main(int , char *[])
{
    Child2 c2;
    c2.SetData(100);
    UseCl uc;
    uc.func(&c2);
    return 0;
}

