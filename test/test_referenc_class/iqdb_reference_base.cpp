#include "stdafx.h"

#include <limits>
#include "iqdb_reference_base.h"

namespace IQDB {

static unsigned int max_uint = (std::numeric_limits<unsigned int>::max)(); //

iqdb_reference_base::iqdb_reference_base()
: self_ref_count_(0)
{
}

iqdb_reference_base::~iqdb_reference_base() {
    assert(0==self_ref_count_);
}

void iqdb_reference_base::reference() const {
    assert(self_ref_count_<max_uint);
    self_ref_count_++;
}

int iqdb_reference_base::dereference() const{
    assert(self_ref_count_>0);
    return --self_ref_count_;
}

} /* IQDB  */
