#include "configureinfo.h"
#include "singleconfig.h"
#include <iostream>

#include <stdio.h>
#include <string>
using namespace std;

int main(int argc, char **argv)
{
    if (argc != 2) {
        std::cout << "error para must be 2" << std::endl;
    } else {
        std::string moduleName = "a.conf";
        std::string err;
        if (SingleConfig::SetConfigPath(argv[1], true, err)) {
            std::cout << "get value = " << SingleConfig::GetStrValue(moduleName.c_str(), "hostlist")
                      << std::endl;
            std::cout << "get value = " << SingleConfig::GetStrValue(moduleName.c_str(), "maxlen")
                      << std::endl;
        } else {
            std::cout << "error info = " << err << std::endl;
            return 0;
        }

        /*std::string filepath = "/home/xmplat/whb/17readconfig/testdir/a.conf";
          FILE * fp = fopen(filepath.c_str(),"a+");
          if(fp != NULL)
          {
          std::string addData = "mypath = /home/test/xmp/whb/1.log";
          SingleConfig::ReloadModule(filepath.c_str());
          std::cout << "get value new= " <<SingleConfig::GetValue("a.conf","mypath") <<
          std::endl;
          fclose(fp);
          }*/

        // std::string modifyFile = argv[1];
        std::string modifyFile = argv[1];
        modifyFile.append("/").append(moduleName);
        cout << "modify file is " << modifyFile << endl;
        SingleConfig::SetDefaultValue(moduleName.c_str(), "int_value", 2147483645);
        SingleConfig::SetDefaultValue(moduleName.c_str(), "double_value", 1234.56789);
        long long dd = 1234234234234;
        SingleConfig::SetDefaultValue(moduleName.c_str(), "long_value", dd);
        SingleConfig::AddAutoReloadFile(modifyFile.c_str());
        SingleConfig::StartAutoReload(3);
        std::cout
        << "get maxlen = " << SingleConfig::GetStrValue(moduleName.c_str(), "maxlen")
        << std::endl;
        std::cout << "begin to sleep" << std::endl;
        sleep(15);
        std::cout
        << "get maxlen = " << SingleConfig::GetStrValue(moduleName.c_str(), "maxlen")
        << std::endl;
        int iNum = SingleConfig::GetIntValue(moduleName.c_str(), "int_value");
        double dNum = SingleConfig::GetDoubleValue(moduleName.c_str(), "double_value");
        long lNum = SingleConfig::GetLongValue(moduleName.c_str(), "long_value");
        std::cout << "获取默认 数字 iNum= " << iNum << "  dNum=" << dNum
                  << "  lNum=" << lNum << std::endl;

        SingleConfig::Release();
    }

    return 0;
}
