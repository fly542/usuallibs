#ifndef __BITMAP_H__
#define __BITMAP_H__
/* pur @ 
 * author @ fly@163.com
 * date @ 2017-08-16
*/

#include <string.h>
class BitMap
{
public:
    BitMap(void* buf, size_t len);
    BitMap(size_t len);
    static long long TestGetNum();

    ~BitMap();

    bool Get(size_t n);
    void Set(size_t n, bool val = true);

    /* pur @ 将bitmap信息拷贝到buf中
     * para @ buf 存放拷贝结果
     * para @ len buf的最大长度
     * return @ true 成功，false失败
     */
    bool ToBuf(void* buf, size_t len);

    /* pur @ 从buf中设置当前bitmap信息
     * para @ buf 要设置bitmap信息
     * para @ len buf的长度
     * return @ true 成功，false失败
     */
    bool FromBuf(void* buf, size_t len);

    /* pur @ 重置当前的bitmap为0 */
    void Reset();

    /* pur @ 获取当前bitmpa的大小*/
    size_t Size();

    /* pur @ 获取当前已经设置的个数 */
    size_t Count();

    /* pur @ 当前bitmap是否已满 */
    bool Full();

    /* pur @ 获取当前bitmap占用的空间大小*/
    size_t GetSpace();
    /**
     * 获取空间数据指针
     */
    unsigned char* GetSpaceData();

    const BitMap& operator=(const BitMap& bitmap);

private:
    //从新统计count数量
    void ReCount();

private:
    unsigned char* m_bmp;
    size_t m_size;
    size_t m_count;

    static acl::atomic_long testNum_;//内存监控测试
};

#endif // __BITMAP_H__
