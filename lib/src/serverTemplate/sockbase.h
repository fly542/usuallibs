/* pur @ 定义sock相关调用类的基类
 * date @ 2013.10.22
 * author @ haibin.wang
*/

#ifndef SOCKBASE_H
#define SOCKBASE_H
#include <string>
/*
 * pur @ 定义sock基类，主要用于server和client的继承共用相关的读写函数和句柄
 * usage @
*/
class SockBase {
    protected:
    int m_fd;          // socket句柄
    std::string m_err; //错误信息

    public:
    SockBase();
    virtual ~SockBase();

    /*
     * pur @ 获取错误信息，当调用CreateConnect出错时再调用此函数可获取错误信息
     * return @ 返回错误信息
    */
    const char *GetErrorInfo();

    /*
     * pur @ 读数据
     * para @ data 存放读取到的数据
     * para @ size 要读取的数据长度
     * para @ timeout 超时时间设置 单位ms, 如果是0则不设置超时时间
     * return @ <0说明错误，否则返回读取到的数据长度, =0 对端关闭连接
    */
    int Read(void *data, int size, int timeout);

    /*
     * pur @ 写数据
     * para @ data 存放读取到的数据
     * para @ size 要读取的数据长度
     * para @ timeout 超时时间设置 单位ms, 如果是0则不设置超时时间
     * return @ <0说明错误，否则返回写的数据长度
    */
    int Write(void *data, int size, int timeout);

    /* pur @ 读取一行数据
     * para @ data 存放读取的数据内容
     * para @ max_len data存放的最大字节数
     * para @ timeout 超时时间 单位ms
     * return @ <0 读取错误，否则返回写的数据长度
    */
    int ReadLine(void *data, int max_len, int timeout);
    /*
     * pur @ 获取句柄
     * return @ 返回socket句柄
     */
    int GetFd()
    {
        return m_fd;
    }

    /*
     * pur @ 关闭打开的连接句柄
     */
    int Close();

    protected:
    /*
     * pur @ 超时监测
     * para @ fd 要监测超时的句柄
     * para @ timeout 超时时间设置 单位ms
     * para @ bRead 是否是检测read，true是，false不是
     * return @ false 未超时，true超时
    */
    static bool TimeoutRWCheck(int fd, int timeout, bool bRead = true);
};

#endif
