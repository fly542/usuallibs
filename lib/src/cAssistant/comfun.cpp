/* purpose @ 常用函数和封装类
 * date    @ 2014.04.01
 * author  @ haibin.wang
 */

#include "comfun.h"

#include <algorithm>
#include <dirent.h>
#include <errno.h>
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <string>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <vector>

//以下为网络相关接口
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <netinet/in.h>

//#include <sys/socket.h>
//#include <sys/types.h>

bool GetDirFiles(const char *dirPath, std::vector<std::string> &files)
{
    if (!IsDir(dirPath)) {
        return false;
    }
    struct dirent *dp;
    DIR *dir = opendir(dirPath);
    if (NULL == dir) {
        return false;
    }
    while (0 != (dp = readdir(dir))) {
        if ((strcmp(dp->d_name, ".") == 0) || (strcmp(dp->d_name, "..") == 0)) {
            continue;
        }
        std::string filePath = dirPath;
        size_t pos = filePath.find_last_of('/');
        if ((pos == std::string::npos) || (pos != (filePath.size() - 1))) {
            filePath.append("/");
        }
        filePath.append(dp->d_name);
        if (!IsDir(filePath.c_str())) {
            files.push_back(filePath);
        }
    }
    closedir(dir);
    return true;
}

std::string GetFileName(const char *filepath)
{
    std::string strName = filepath;
    size_t pos = strName.rfind('/');
    if (pos == std::string::npos) {
        return strName;
    } else {
        return strName.substr(pos + 1);
    }
}

std::string FormatPath(const char *path)
{
    std::string tmp;
    char *p = (char *)path;
    while (*p) {
        if ('\0' != *(p + 1)) {
            if ('/' == *p && '/' == *(p + 1)) {
                p++;
                continue;
            } else if ('\\' == *p) {
                if ('\\' == *(p + 1)) {
                    p++;
                    continue;
                }
                tmp.append(1, '/');
            } else {
                tmp.append(1, *p);
            }
        } else {
            if ('\\' == *p) {
                tmp.append(1, '/');
            } else {
                tmp.append(1, *p);
            }
        }
        p++;
    }
    return tmp;
}

bool IsDir(const char *path)
{
    struct stat curStat;
    if (0 == stat(path, &curStat)) {
        if (S_ISDIR(curStat.st_mode)) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

/* pur @ 字符串分隔函数
 * para @ str 要分隔的字符串
 * para @ pattern 分隔符，不能按照多个分隔符分隔
 * para @ result 返回分隔后的字符串列表，顺序按照str的先后顺序存放
*/
void Split(const std::string &str, const std::string &pattern, std::vector<std::string> &result)
{
    std::string::size_type pos;
    std::string tmp = str;
    tmp += pattern; //扩展字符串以方便操作
    size_t size = tmp.size();
    for (size_t i = 0; i < size; i++) {
        pos = tmp.find(pattern, i);
        if (pos < size) {
            std::string s = tmp.substr(i, pos - i);
            if (!s.empty()) {
                result.push_back(s);
            }
            i = pos + pattern.size() - 1;
        }
    }
}

void TrimLine(std::string &line)
{
    line.erase(line.begin(),
               std::find_if(line.begin(), line.end(), std::not1(std::ptr_fun(::isspace))));
    line.erase(
    std::find_if(line.rbegin(), line.rend(), std::not1(std::ptr_fun(::isspace))).base(),
    line.end());
}

bool MMAPFile(int openFd, size_t fileLen, MapData &mpData)
{
    mpData.m_length = fileLen;
    void *pMM = NULL;
    pMM = mmap(NULL, (size_t)fileLen, PROT_READ, MAP_PRIVATE, openFd, 0);
    if (pMM == MAP_FAILED) {
        return false;
    }

    mpData.m_data = pMM;
    if (pMM) {
        return true;
    } else {
        return false;
    }
}

//以下函数取消了函数的映射
void UnMMAPFile(MapData &mpData)
{
    if (mpData.m_data) {
        munmap(mpData.m_data, mpData.m_length);
    }
}

/******************************************************************************
 *
 * 以下为网络实用函数定义
 *
 ******************************************************************************/

/* pur @ 检查addr格式的正确性，格式为ip:port,ip1:port1,ip2:port2
 * para @ addr ip:port地址列表
 * return @ true符合ip格式要求，false, 不符合格式要求
*/
bool CheckSockAddr(const char *addr)
{
    std::vector<std::string> vecAddrs;
    Split(addr, ",", vecAddrs);
    for (unsigned int i = 0; i < vecAddrs.size(); i++) {
        std::vector<std::string> vecIp;
        Split(vecAddrs[i].c_str(), ":", vecIp);
        if (2 != vecIp.size()) {
            return false;
        } else {
            int port = atoi(vecIp[1].c_str());
            if (port <= 0 || vecIp[0].empty()) {
                return false;
            }
        }
    }
    return true;
}

void GetLocalIps(std::vector<std::string> &ips, bool bIpV6)
{
    struct ifaddrs *ifAddrStruct = NULL, *tmpAddrs = NULL;
    void *tmpAddrPtr = NULL;
    getifaddrs(&ifAddrStruct);
    if (NULL == ifAddrStruct) {
        return;
    } else {
        tmpAddrs = ifAddrStruct; //指向开始位置
    }
    while (ifAddrStruct != NULL) {
        if (ifAddrStruct->ifa_addr->sa_family == AF_INET && !bIpV6) {
            tmpAddrPtr = &((struct sockaddr_in *)ifAddrStruct->ifa_addr)->sin_addr;
            char addressBuffer[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
            if (0 != strcmp(addressBuffer, "127.0.0.1")) {
                ips.push_back(addressBuffer);
            }
        } else if (ifAddrStruct->ifa_addr->sa_family == AF_INET6 && bIpV6) {
            tmpAddrPtr = &((struct sockaddr_in *)ifAddrStruct->ifa_addr)->sin_addr;
            char addressBuffer[INET6_ADDRSTRLEN];
            inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
            if (0 != strcmp(addressBuffer, "::")) {
                ips.push_back(addressBuffer);
            }
        }
        ifAddrStruct = ifAddrStruct->ifa_next;
    }
    if (tmpAddrs) {
        freeifaddrs(tmpAddrs);
    }
}

bool IsExist(const char *path)
{
    bool bRet = true;
    if ((-1 == access(path, F_OK)) || (-1 == access(path, R_OK))) {
        bRet = false;
    }
    return bRet;
}

unsigned long FileSize(const char *path)
{
    unsigned long filesize = 0;
    struct stat statbuff;
    if (stat(path, &statbuff) < 0) {
        return filesize;
    } else {
        filesize = statbuff.st_size;
    }
    return filesize;
}

bool Mkdir(const char *path)
{
    bool bRet = false;
    if (0 == mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)) {
        bRet = true;
    }
    return bRet;
}

bool Mkdirs(const char *path, int mode)
{
    const char *tp = path;
#define SKIP_WHILE(cond, ptr)                                                            \
    {                                                                                    \
        while (*ptr && (cond))                                                           \
            ptr++;                                                                       \
    }
    SKIP_WHILE(*tp == '/', tp);
    while (*tp != 0) {
        if (*tp != '/') {
            tp++;
            continue;
        }
        std::string tmpPath(path, tp - path);
        struct stat st;
        if (0 == stat(tmpPath.c_str(), &st)) {
            if (!S_ISDIR(st.st_mode)) {
                return false;
            }
        } else {
            if (errno != ENOENT) {
                return false;
            }
            if (0 != mkdir(tmpPath.c_str(), mode)) { //创建目录
                if (errno != EEXIST) {
                    return false;
                }
                struct stat st2;
                if (0 != stat(tmpPath.c_str(), &st2)) {
                    return false;
                }
                if (!S_ISDIR(st2.st_mode)) {
                    return false;
                }
            }
        }
        tp++;
    }
    return true;
}


/*-----------------------------------------
 * 局域网IP地址范围
 * A类：10.0.0.0-10.255.255.255
 * B类：172.16.0.0-172.31.255.255 
 * C类：192.168.0.0-192.168.255.255
 * -------------------------------------------*/
bool CheckLocalIp(const std::string& ipstring)
{
    std::istringstream st(ipstring);
    int ip[2];
    for(int i = 0; i < 2; i++)
    {
        std::string temp;
        getline(st,temp,'.');
        std::istringstream a(temp);
        a >> ip[i];
    }
    if((ip[0]==10) ||
            (ip[0]==172 && ip[1]>=16 && ip[1]<=31) ||
            (ip[0]==192 && ip[1]==168)) {
        return true;
    } else {
        return false;
    }
}

