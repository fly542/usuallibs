#!/bin/bash
################################################################################
#  Copyright (C) 2015-2018 IQIYI All rights reserved.
# 
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Sun 07 Feb 2021 09:17:14 AM CST
#  Destription: 开发机rm命令替换脚本
#           1. 在用户目录下建立 .trash 和 .tools 目录
#                mkdir -p ~/.trash  ~/.tools
#           2. 将此脚本拷贝至~/.tools/ 目录下
#           3. 修改~/.bashrc， 增加一行 
#               alias rm="sh /root/.tools/remove.sh"
#           4. 设置crontab，定期清空垃圾箱，如：(每天0点清空垃圾箱)
#               0 0 * * * rm -rf /root/.trash/*
#
################################################################################
#cd `dirname $0`

#  是否显示执行过程
#set -x 

PARA_CNT=$#
# 被删除文件的存储目录
TRASH_DIR="/root/.trash"

for i in $*; do
    if [ ${i} = "-rf" ] || [ ${i} = "-r" ] || [ ${i} = "-f" ]                      
    then                                                                            
        continue                                                                    
    fi 
    STAMP=`date +%s`
    fileName=`basename $i`
    mv $i $TRASH_DIR/$fileName.$STAMP
done
