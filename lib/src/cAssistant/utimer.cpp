/* purpose @ ΢���ʱ����
 * date    @ 2014.04.01
 * author  @ haibin.wang
 */

#include "utimer.h"
#include <string.h>

UTimer::UTimer()
{
    memset(&m_begin, 0, sizeof(struct timeval));
    memset(&m_end, 0, sizeof(struct timeval));
}

UTimer::~UTimer()
{
}

void UTimer::Start()
{
    gettimeofday(&m_begin, NULL);
}

long long UTimer::StopU()
{
    gettimeofday(&m_end, NULL);
    return (m_end.tv_sec - m_begin.tv_sec) * 1000000 + (m_end.tv_usec - m_begin.tv_usec);
}

long long UTimer::StopM()
{
    gettimeofday(&m_end, NULL);
    return (m_end.tv_sec - m_begin.tv_sec) * 1000 + (m_end.tv_usec - m_begin.tv_usec) / 1000;
}

long long UTimer::StopS()
{
    gettimeofday(&m_end, NULL);
    return m_end.tv_sec - m_begin.tv_sec;
}
