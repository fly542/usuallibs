# openssl des -salt -in hei.txt -out hei.dat
PID=$$
if [ $1 ]
  then
    record=`cat ./list | grep -v "^#" | awk /"$1"/'{print}'` 
  else
    cat ./list | awk '{$2="";$4="";print $0}'
    exit 0
fi
xuser=`echo $record|awk '{print $2}'`
xip=`echo $record|awk '{print $3}'`
xpass1=`echo $record | awk '{print $4}'`
#echo $xip
if [  $xip ]
  then
    > /tmp/go.$PID.tcl
    echo "#!/usr/bin/expect -f"										>> /tmp/go.$PID.tcl
    echo "set timeout 100"										>> /tmp/go.$PID.tcl
    echo "spawn /usr/bin/ssh -l " $xuser " " $xip							>> /tmp/go.$PID.tcl
    echo "expect {"                                                                                 	>> /tmp/go.$PID.tcl
    echo "          \"yes\"                 {send \"yes\r\";expect assword: ;send \"$xpass1\r\"}"   	>> /tmp/go.$PID.tcl
    echo "          \"assword:\"            {send \"${xpass1}\r\"}"                                   	>> /tmp/go.$PID.tcl
    echo "          \"ASSWORD:\"            {send \"$xpass1\r\"}"                                   	>> /tmp/go.$PID.tcl
    echo "          \"#\"                   {send \"\r\"}"                                          	>> /tmp/go.$PID.tcl
    echo "  }"                                                                                      	>> /tmp/go.$PID.tcl
    if [ $xuser = 'dep' ]
      then
#        echo 'expect ' $xuser		>> /tmp/go.$PID.tcl
#        echo "send "'"'sudo su -\\r'"'	>> /tmp/go.$PID.tcl
#        echo 'expect root'		>> /tmp/go.$PID.tcl
#	echo "send "'"'export GREP_OPTIONS="--color=auto"\\r'"'>>/tmp/go.$PID.tcl
#        echo "interact"			>> /tmp/go.$PID.tcl
#      else
       #echo 'expect \#'		>> /tmp/go.$PID.tcl
        echo "interact"			>> /tmp/go.$PID.tcl
    fi
    chmod 500 /tmp/go.$PID.tcl
    expect -f /tmp/go.$PID.tcl
    #echo $PID
    #rm -f go.$PID.tcl
    #expect -f go.$PID.tcl "$xip" "$xuser" "$xpass1" "$xpass2"
fi
