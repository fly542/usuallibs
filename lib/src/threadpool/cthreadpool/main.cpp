#include "threadpool.h"
#include <iostream>
#include <string.h>
#include <string>
#include <sys/time.h>

#include <pthread.h>

using namespace std;
static int seq = 0;
pthread_mutex_t m_mutex;
void myFunc(void *arg)
{
    pthread_mutex_lock(&m_mutex);
    seq++;
    cout << "start-Func=" << seq << endl;
    pthread_mutex_unlock(&m_mutex);
    // sleep();
}

int main(int argc, char **argv)
{
    pthread_mutex_init(&m_mutex, NULL);
    struct timeval time_beg, time_end;
    memset(&time_beg, 0, sizeof(struct timeval));
    memset(&time_end, 0, sizeof(struct timeval));
    gettimeofday(&time_beg, NULL);
    threadpool pool = create_threadpool(100);
    cout << "*******************初始化完成*********************" << endl;
    for (int i = 0; i < 1000000; ++i) {
        while (0 != dispatch_threadpool(pool, myFunc, NULL)) {
            usleep(100);
        }
    }
    destroy_threadpool(pool);
    // sleep(10);
    cout << __FILE__ << "将关闭所有线程" << endl;
    gettimeofday(&time_end, NULL);
    long total =
    (time_end.tv_sec - time_beg.tv_sec) * 1000000 + (time_end.tv_usec - time_beg.tv_usec);
    cout << "total time =" << total << endl;
    // pthread_mutex_destroy(&m_mutex);
    return 0;
}
