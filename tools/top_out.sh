#!/bin/bash
################################################################################
#  Copyright (C) 2022 IQIYI All rights reserved.
#
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Fri 08 Jul 2022 04:10:36 PM CST
#  Destription: 将top信息输出到文件中
#           运行方式:
#                   nohup ./top_out.sh &
#
################################################################################
cd `dirname $0`

#  是否显示执行过程
#set -x 
# 每隔2秒 输出到a.log中
top -p $(pgrep xxx -d,) -d2 -b >a.log
exit 0;

