/**
 * Copyright (C) 2015-2018 IQIYI
 * All rights reserved.
 *
 * AUTHOR(S)
 * 王海斌
 *   E-mail: wanghaibin@qiyi.com
 * 
 * VERSION
 *   Thu 26 Apr 2018 10:55:44 AM CST
 */

#include <iostream>
#include <string>

using namespace std;
int main(int argc, char *argv[])
{
	string test("abc123_100_200.ts");
	cout << "before=" << test << endl;
	test.replace(test.find("_"), test.length(), ".f4v");
	cout << "end=" << test <<endl;
	string test2("abc123.ts");
	cout << "before=" << test2 << endl;
	test2.replace(test2.find("."), 1, ".f4v");
	cout << "end=" << test2 <<endl;
	return 0;
}
