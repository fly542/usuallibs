#!/bin/bash
################################################################################
#  Copyright (C) 2023 IQIYI All rights reserved.
#
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Sun 01 Jan 2023 09:02:50 AM CST
#  Destription: 清理因为postfix待删除邮件把系统根目录存满的情况
#
#        关闭cron发送邮件解决方案：
#        在cron的第一行加入 MAILTO="" 便可，这样执行当前用户的Cron时，不会发送邮件。
################################################################################
cd `dirname $0`

#  是否显示执行过程
#set -x 

cd /var/spool/postfix/maildrop
ls | xargs -r rm -rf

# 关闭postfix服务
chkconfig postfix off
