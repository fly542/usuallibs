#!/bin/bash
################################################################################
#  Copyright (C) 2023 IQIYI All rights reserved.
#
#  Author     : 王海斌
#  E-mail     : wanghaibin@qiyi.com
#  Version    :
#  Date       : Wed 26 Apr 2023 03:01:36 PM CST
#  Destription: 根据openssl源码创建对应的rpm包
#               注意当前目录下要有对应openssl的tar.gz的源码包
#               perl-WWW-Curl 为openssl编译后的rpm需要依赖的包,
################################################################################

cd `dirname $0`

#  是否显示执行过程
#set -x 

set -e
set -v
yum -y install \
    which \
    make \
    gcc \
    perl \
    perl-WWW-Curl \
    rpm-build

build_dir=./openssl_rpmbuild/
spec_file=SPECS/openssl.spec
mkdir -p ${build_dir}/{BUILD,RPMS,SOURCES,SPECS,SRPMS}

# create SPEC file
cat << 'EOF' > ${build_dir}/${spec_file}
Summary: OpenSSL 1.1.1q for Centos
Name: openssl
Version: %{?version}%{!?version:1.1.1q}
Release: 1%{?dist}
Obsoletes: %{name} <= %{version}
Provides: %{name} = %{version}
URL: https://www.openssl.org/
License: GPLv2+
Source0: %{name}-%{version}.tar.gz

BuildRequires: make gcc perl perl-WWW-Curl
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
%global openssldir /usr/local

%description
OpenSSL RPM for version 1.1.1q on Centos

%package devel
Summary: Development files for programs which will use the openssl library
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
OpenSSL RPM for version 1.1.1q on Centos (development package)

%prep
%setup -q

%build
./config --prefix=%{openssldir} --openssldir=%{openssldir}
make -j16

%install
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}
%make_install

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%files
%{openssldir}
%defattr(-,root,root)

%files devel
%{openssldir}/include/*
%defattr(-,root,root)

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

EOF


cp ./openssl-1.1.1q.tar.gz ${build_dir}/SOURCES
cd ${build_dir}
rpmbuild  -D "_topdir ${PWD}" -D "_sourcedir ${PWD}/SOURCES" \
    -D "_rpmdir ${PWD}/RPMS" -D "version 1.1.1q" -ba ${spec_file}

# Before Uninstall  Openssl :   rpm -qa openssl
# Uninstall Current Openssl Vesion : yum -y remove openssl
# For install:  rpm -ivvh ./openssl_rpmbuild/RPMS/x86_64/openssl-1.1.1q-1.el7.x86_64.rpm --nodeps
# Verify install:  rpm -qa openssl
#                  openssl version
