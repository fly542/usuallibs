//=============================================================================
/**
 *  @file    datetime.h
 *
 *  ver 1.0.0 Reg Exp, for Reg Develop
 *
 *  @author Reg  2011/01/12 created.
 */
//=============================================================================

#ifndef DATETIME_H
#define DATETIME_H

#include <string>
#include <time.h>
using namespace std;

class CDateTime {
    protected:
    time_t m_tTime;
    struct tm m_stTime;

    public:
    CDateTime();
    CDateTime(time_t tTime);
    CDateTime(const struct tm &stTime);
    CDateTime(int iYear, int iMonth, int iDay, int iHour = 0, int iMinute = 0, int iSecond = 0);

    time_t GetValue()
    {
        return m_tTime;
    }
    const struct tm &GetStruct()
    {
        return m_stTime;
    }

    int Year()
    {
        return m_stTime.tm_year + 1900;
    }
    int Month()
    {
        return m_stTime.tm_mon + 1;
    }
    int Day()
    {
        return m_stTime.tm_mday;
    }
    int Hour()
    {
        return m_stTime.tm_hour;
    }
    int Minute()
    {
        return m_stTime.tm_min;
    }
    int Second()
    {
        return m_stTime.tm_sec;
    }
    int DayOfWeek()
    {
        return m_stTime.tm_wday;
    }
    int DayOfYear()
    {
        return m_stTime.tm_yday;
    }

    string ShortDateTime();
    string LongDateTime(char chSep1 = '-', char chSep2 = ':');
    string ShortDate();
    string LongDate(char chSep = '-');
};

#endif
