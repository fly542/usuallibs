#include "connectpool.h"
#include <pthread.h>
#include <stdlib.h>

#include "comfun.h"
#include "simplesock.h"

SockHandle::SockHandle() : m_fd(-1), m_time(0)
{
}

SockHandle::SockHandle(int fd, time_t tmpTime) : m_fd(fd), m_time(tmpTime)
{
}

PoolClient::PoolClient() : m_addr(""), m_port(-1), m_count(0), m_stop(false)
{
}

ConnectInfo::ConnectInfo() : m_addr(""), m_port(-1), m_fd(-1)
{
}

ConnectPool::ConnectPool()
: m_max(5), m_used(0), m_curSeq(0), m_AutoReleaseTime(0), m_bStartAutoRelease(false),
  m_errInfo("")
{
}

ConnectPool::~ConnectPool()
{
    ReleasePool();
    pthread_mutex_destroy(&m_lock);
}

PoolClient *ConnectPool::CreatePoolClient(const char *addr, int port)
{
    PoolClient *cli = NULL;
    cli = new PoolClient;
    if (NULL != cli) {
        cli->m_addr = addr;
        cli->m_port = port;
    }
    return cli;
}

int ConnectPool::CreateConnect(PoolClient *client)
{
    ClientSock clientsock;
    return clientsock.CreateConnect(client->m_addr.c_str(), client->m_port, 10000);
}

bool ConnectPool::InitPool(const char *addr, const int max, const int preCount)
{
    // 1、检查ip地址格式是否正常
    if (!CheckSockAddr(addr)) {
        m_errInfo = "Ip address is error!";
        return false;
    } else {
        if (preCount < 0 || max <= 0 || preCount > max) {
            m_errInfo = "parameter error！";
            return false;
        }
        if (max > m_max) {
            m_max = max;
        }

        std::vector<std::string> vecAddrs;
        Split(addr, ",", vecAddrs);
        for (size_t i = 0; i < vecAddrs.size(); i++) {
            std::vector<std::string> vecIp;
            Split(vecAddrs[i].c_str(), ":", vecIp);
            PoolClient *cli = NULL;
            while (NULL == cli) {
                cli = CreatePoolClient(vecIp[0].c_str(), atoi(vecIp[1].c_str()));
            }
            m_pool.push_back(cli);
            if (preCount > 0) {
                for (int j = 0; j < preCount; ++j) {
                    int fd = CreateConnect(cli);
                    if (fd != -1) {
                        cli->m_fds.push_front(SockHandle(fd, time(NULL)));
                        cli->m_count = cli->m_count + 1;
                    }
                }
            }
        }
        pthread_mutex_init(&m_lock, NULL);
        return true;
    }
}

bool ConnectPool::SetAutoReleaseTime(const int rTime)
{
    if (rTime > 0) {
        m_AutoReleaseTime = rTime;
        if (!m_bStartAutoRelease) {
            pthread_t threadid;
            pthread_attr_t attr;
            pthread_attr_init(&attr);
            pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
            pthread_create(&threadid, &attr, ConnectPool::CheckAutoRelease, this);
            pthread_attr_destroy(&attr);
        }
        return true;
    }
    m_errInfo = "set auto release time must >=0";
    return false;
}

void ConnectPool::ReleasePool()
{
    LockPool();
    std::vector<PoolClient *>::iterator it = m_pool.begin();
    for (; it != m_pool.end(); ++it) {
        std::list<SockHandle>::iterator itlist = (*it)->m_fds.begin();
        for (; itlist != (*it)->m_fds.end(); ++itlist) {
            close((*itlist).m_fd);
        }
    }
    m_pool.clear();
    UnLockPool();
}

ConnectInfo *ConnectPool::CreateConnectInfo(const std::string &addr, const int port, const int fd)
{
    ConnectInfo *curInfo = NULL;
    curInfo = new ConnectInfo;
    if (curInfo != NULL) {
        curInfo->m_addr = addr;
        curInfo->m_port = port;
        curInfo->m_fd = fd;
    }
    return curInfo;
}

#include <iostream>
ConnectInfo *ConnectPool::GetConnectInfo()
{
    ConnectInfo *curInfo = NULL;
    int iSize = m_pool.size();
    for (int i = 0; i < iSize; ++i) {
        int pos = i + m_curSeq % iSize;
        PoolClient *pClient = m_pool[pos];
        if (!pClient->m_stop) {
            if (!pClient->m_fds.empty()) {
                std::list<SockHandle>::iterator it = pClient->m_fds.begin();
                curInfo = CreateConnectInfo(pClient->m_addr, pClient->m_port, (*it).m_fd);
                if (curInfo != NULL) {
                    pClient->m_fds.erase(it);
                    m_used++;
                    m_curSeq = pos;
                    break;
                }
            } else {
                if (pClient->m_count < m_max) // 创建连接
                {
                    // std::cout << "current count = " << pClient->m_count << std::endl;
                    int fd = CreateConnect(pClient);
                    if (fd != -1) {
                        curInfo = CreateConnectInfo(pClient->m_addr, pClient->m_port, fd);
                        if (curInfo != NULL) {
                            m_used++;
                            m_curSeq = pos;
                            pClient->m_count = pClient->m_count + 1;
                            break;
                        }
                    }
                }
            }
        }
    }
    return curInfo;
}

ConnectInfo *ConnectPool::GetConnect()
{
    LockPool();
    ConnectInfo *curInfo = NULL;
    if (m_pool.empty()) {
        return NULL;
    }

    if (m_curSeq > m_pool.size()) {
        m_curSeq = 0;
    }

    if (m_pool[m_curSeq]->m_stop) {
        m_curSeq += 1;
    }
    curInfo = GetConnectInfo();
    UnLockPool();
    return curInfo;
}

void ConnectPool::BackToPool(ConnectInfo *conn, bool bConnect)
{
    if (NULL == conn) {
        return;
    }
    LockPool();
    //将连接收回
    for (size_t i = 0; i < m_pool.size(); ++i) {
        PoolClient *pClient = m_pool[i];
        if ((0 == pClient->m_addr.compare(conn->m_addr)) && (pClient->m_port == conn->m_port)) {
            if (bConnect) {
                pClient->m_fds.push_front(SockHandle(conn->m_fd, time(NULL)));
            } else {
                int confd = CreateConnect(pClient);
                if (confd != -1) {
                    pClient->m_fds.push_front(SockHandle(confd, time(NULL)));
                } else {
                    pClient->m_count = pClient->m_count - 1;
                }
            }
            --m_used;
            delete conn;
            conn = NULL;
            break;
        }
    }
    //收回后监测是否到了回收连接时间
    // CheckAutoRelease();
    UnLockPool();

    if (NULL != conn) {
        close(conn->m_fd);
        delete conn;
        conn = NULL;
    }
}

void ConnectPool::LockPool()
{
    pthread_mutex_lock(&m_lock);
}

void ConnectPool::UnLockPool()
{
    pthread_mutex_unlock(&m_lock);
}
std::vector<PoolClient *> &ConnectPool::GetPool()
{
    return m_pool;
}

int ConnectPool::GetAutoReleaseTime()
{
    return m_AutoReleaseTime;
}

void *ConnectPool::CheckAutoRelease(void *para)
{
    ConnectPool *pool = (ConnectPool *)para;
    if (pool != NULL) {
        while (1) {
            if (pool->GetAutoReleaseTime() > 0) {
                time_t tNow = time(NULL);
                pool->LockPool();
                std::vector<PoolClient *> m_pool = pool->GetPool();
                for (size_t i = 0; i < m_pool.size(); ++i) {
                    PoolClient *pClient = m_pool[i];
                    std::list<SockHandle>::iterator it, next;
                    std::list<SockHandle>::reverse_iterator rit = pClient->m_fds.rbegin();
                    while (rit != pClient->m_fds.rend()) {
                        it = --rit.base();
                        if (tNow - (*it).m_time >= pool->GetAutoReleaseTime()) {
                            close((*it).m_fd);
                            next = pClient->m_fds.erase(it);
                            rit = std::list<SockHandle>::reverse_iterator(next);
                            pClient->m_count -= 1;
                        } else {
                            break;
                        }
                    }
                }
                pool->UnLockPool();
            }
            sleep(CHECK_AUTO_RELEASE_INTERVAL);
        }
    }
    return NULL;
}
