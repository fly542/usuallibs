| 工具名                | 说明                      | 用法                                       |
|-----------------------|---------------------------|--------------------------------------------|
| clear_postfix_mail.sh | 清理postfix目录下垃圾文件 | nohup ./clear_postfix_mail.sh &            |
| shellTmp.sh           | shell文件模板文件         |                                            |
| loop_dir.sh           | 遍历目录下的文件          |                                            |
| valgrind.sh           | valgrind 检测程序常用脚本 |                                            |
| dev_env/gitconfig     | git常用配置文件           | 拷贝文件为用户下的.gitconfig               |
| dev_env/vimrc 常用vim配置文件 | 拷贝文件到用户下的.vimrc  | 然后运行vim 输入PluginInstall 安装相关插件 |
| login                 | linxu下跳转登录脚本       | 修改list文件为对应用户名和密码             |
| git-archive-all.sh    | 递归获取目录下所有子模块代码  | 在项目目录下执行 脚本./git-archive-all.sh            |
| top_out.sh            | 每隔几秒输出top信息到文件中 | nohup ./top_out.sh &            |
| get_core_file_from_ci.sh    | 在ci中获取崩溃时的core文件和对应的二进制文件|                     |
| remote_exec.sh    | 在多个机器上批量执行简单的命令|   在跳板机上执行此脚本                  |
| create_openssl_rpm.sh    | 通过当前目录下的openssl源码包编译出openssl的rpm包|   执行脚本即可,当前录下的openssl_rpmbuild/RPMS/x86_64/下将生成openssl和openssl-devel的rpm包                 |

