/**
 * Copyright (C) 2015-2018 IQIYI
 * All rights reserved.
 *
 * AUTHOR(S)
 * Wang Haibin
 *   E-mail: wanghaibin@qiyi.com
 * 
 * VERSION
 *   Wed 18 Oct 2017 10:02:36 PM CST
 */

/**
 * 检测系统是大端还是小端对齐，网络字节序是大端对齐
 * @param 
 * @param 
 * @return  0 未知的字节序，1 大端对齐，2 小端对齐
 */
int CheckByteOrder();
