#include <algorithm>
#include <arpa/inet.h> //inet_pron
#include <iostream>
#include <netinet/in.h> // socket
#include <netinet/tcp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> //bzero
#include <sys/select.h>
#include <sys/socket.h> //socket
#include <sys/types.h>  //AF_INIT
#include <unistd.h>     //close

#define PORT 10008

#include "connectpool.h"
#include <vector>

using namespace std;

int main(int argc, char **argv)
{
    if (argc != 2) {
        printf("useage:%s<ip address>\n", argv[0]);
        return 0;
    }

    int count = 20;
    int preCount = count - 10;
    ConnectPool cpool;
    if (cpool.InitPool(argv[1], count, preCount)) {
        cpool.SetAutoReleaseTime(5);
        vector<ConnectInfo *> vec;

        for (int i = 0; i < count; ++i) {
            if (i >= preCount) {
                // sleep(3);
            }
            ConnectInfo *con = cpool.GetConnect();
            if (con != NULL) {
                vec.push_back(con);
            } else {
                cout << "get connect failed pool is empty !" << endl;
            }
        }
        cout << "back to pool" << endl;
        for (size_t i = 0; i < vec.size(); ++i) {
            // sleep(1);
            cpool.BackToPool(vec[i], true);
        }
        vec.clear();
        sleep(8);

        ConnectInfo *con = cpool.GetConnect();
        if (con) {
            cout << "to auto clear" << endl;
            cpool.BackToPool(con, true);
        }
    } else {
        cout << "Init pool failed! " << cpool.GetErrInfo() << endl;
    }

    cpool.ReleasePool();
}

/*
void process(FILE*fp, ClientSock * mysock);
char * getMessage(char*sendline, int len, FILE*fp);



int oldmain(int argc, char** argv)
{
    int connectfd;

    if(argc != 2)
    {
        printf("useage:%s<ip address>\n", argv[0]);
        return 0;
    }
    ClientSock mysock;
    connectfd = mysock.CreateConnect(argv[1], PORT, 3000);

    if(connectfd >0)
    {
        process(stdin, &mysock);
        mysock.CloseConnect();
    }
    else
    {
        printf("connect error %s\n", mysock.GetErrorInfo());
    }
}
#define MAXDATASIZE 1024
void process(FILE*fp, ClientSock * mysock)
{
    char sendline[MAXDATASIZE], recvline[MAXDATASIZE];
    int numberbytes;
    printf("Input name:");
    if(NULL==fgets(sendline, MAXDATASIZE, fp))
    {
        printf("\nExit!\n");
        return;
    }
    mysock->Write(sendline,strlen(sendline),0);

    fd_set rset, wset;
    FD_ZERO(&rset);
    FD_ZERO(&wset);
    int maxfd;
    //while(NULL!=getMessage(sendline,MAXDATASIZE,fp))
    while(true)
    {
        printf("input string to server111:");
        printf("input string to server:\n");
        FD_SET(fileno(fp), &rset);
        FD_SET(mysock->GetHandle(), &rset);
        FD_SET(fileno(fp), &wset);
        maxfd = std::max(fileno(fp),mysock->GetHandle()) + 1;
        select(maxfd,&rset, NULL, NULL, NULL);
        if(FD_ISSET(mysock->GetHandle(), &rset))
        {
            if(0==(numberbytes=mysock->Read( recvline, strlen(sendline)-1, 0)))
            {
                printf("server terminated \n");
                break;
            }
        }

        if(FD_ISSET(fileno(fp), &wset))
        {
            if(NULL==getMessage(sendline,MAXDATASIZE,fp))
            {
                break;
            }
            mysock->Write(sendline,strlen(sendline),0);
            printf("after write, leng=%d\n",strlen(sendline));

            if(0==(numberbytes=mysock->Read( recvline, strlen(sendline)-1, 0)))
            {
                printf("server terminated \n");
                return ;
            }
            recvline[numberbytes]='\0';
            printf("server message is:%s\n", recvline);
        }
    }
    printf("\nExist!\n");
}

char * getMessage(char*sendline, int len, FILE*fp)
{
    return(fgets(sendline, len, fp));
}
*/
