#ifndef PROCMANAGER_H
#define PROCMANAGER_H
/* purpose @ 进程管理相关函数,
 * date    @ 2014.09.03
 * author  @ haibin.wang
 */

/* pur @使当前进程变为守护进程
 * usage:
 *		....
 *		main()
 *		{
 *			MakeDaemon();
 *			while(true)
 *			{
 *				sleep(1);
 *			}
 *		}
 */
void MakeDaemon(void);

/* pur @ 启动子进程
 * para @ pid //子进程id
 * para @ command 要启动的进程的全路径和参数列表串
 * usage :
 *		int pid =0;
 *		const char * command="/home/testProc/test0 aaaa bbbb ccc"
 *		StartProc(&pid, command);
 */
void StartProc(int *pid, const char *command);

#endif
