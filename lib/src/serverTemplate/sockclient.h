/*
 * pur @ socket客户端类
 * date @ 2013.10.22
 * author @ haibin.wang
 * usage @  ClientSock mysock;
            mysock.connect(10.1.10.100, 18888, 3000);
            mysock.Read(data, 100, 0);
            mysock.Write(data, 100, 0);
            mysock.CloseConnect();
*/

#ifndef CLIENTSOCK_H
#define CLIENTSOCK_H

#include "sockbase.h"
class ClientSock : public SockBase {
    public:
    ClientSock();
    virtual ~ClientSock();

    /*
     * pur @ 创建连接,使用非阻塞方式创建连接
     * addr @ 连接地址
     * port @ 连接端口
     * timeout @ 超时时间，单位ms, 设置为0则永远等待，
     * return @ 成功返回连接句柄，-1则错误，调用GetErrorInfo获取错误信息
    */
    int CreateConnect(const char *addr, int port, int timeout);

    /*
     * pur @ 关闭连接，
     * return @ 0正常关闭，-1 错误,通过GetErrorInfo获取错误信息
    */
    int CloseConnect();

    private:
    /*
     * pur @ 设置socket为是否为非阻塞
     * para @ fd ，要设置的socket句柄
     * para @ bAsync 是否非阻塞，true为非阻塞，false为阻塞
    */
    int SetAsync(int fd, bool bAsync = true);

    /* pur @ 连接超时等待
     * para @ sockfd 要处理的句柄
     * para @ timeout 超时时间
     * return @ <0连接超时或异常, 有数据可读或可写
     */
    int ConnectPollWait(int sockfd, int timeout);

    /* pur @ 设置句柄参数函数
     * para @ fd 要设置的socket句柄
     */
    void SetConnectOpt(int fd);
};
#endif
