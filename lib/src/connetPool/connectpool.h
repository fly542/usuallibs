/* purpose @连接池创建类，通过ConnectPool.InitPool()函数创建连接
 *          通过调用GetConnect获取连接信息
 *          使用完毕后调用BackToPool将连接归还给连接池
 *      功能：
 *          1、初始化连接池，并指定初始时预创建连接的个数，否则在实际获取连接的时候才创建连接
 *          2、创建的连接数最多为初始化时指定的最大值，当达到最大值的时候则直接返回NULL
 *          3、可以指定多个, 每个ip:port均max个连接
 *          4、自动释放超时未使用连接（如果指定了释放连接时间，则超过这个时间未使用的连接将被自动释放）
 *      说明：
 *          1、当第一次设置连接自动销毁时间大于0的时候启动释放连接线程
 *          2、当要单独使用连接池的时候去掉对simplesock的依赖，只需要CreateConnect函数为自己的连接函数即可
 *
 * date    @ 2013.11.29
 * author  @ haibin.wang
 */

#ifndef CONNECTPOOL_H
#define CONNECTPOOL_H
#include <list>
#include <string>
#include <time.h>
#include <vector>

#define CHECK_AUTO_RELEASE_INTERVAL 60 //自动释放连接线程监测释放连接的时间间隔

class SockHandle {
    public:
    SockHandle();
    SockHandle(int fd, time_t tmpTime);
    virtual ~SockHandle()
    {
    }

    public:
    int m_fd;      //连接句柄
    time_t m_time; //句柄访问时间
};

/**
 * 存放一个ip中所有的长连接句柄
 */
class PoolClient {
    public:
    PoolClient();
    virtual ~PoolClient()
    {
    }

    public:
    std::string m_addr;          //连接地址
    int m_port;                  //连接端口
    int m_count;                 //已创建连接数
    bool m_stop;                 //是否下线
    std::list<SockHandle> m_fds; //长连接句柄列表
};

/**
 * 返回给调用者的单个连接句柄
 */
class ConnectInfo {
    public:
    ConnectInfo();
    ~ConnectInfo()
    {
    }

    public:
    std::string m_addr; //连接地址
    int m_port;         //连接端口
    int m_fd;           //长连接句柄
};

class ConnectPool {
    protected:
    int m_max;                        // 连接池最大连接数
    int m_used;                       // 当前使用的连接数
    size_t m_curSeq;                  //当前轮询序号
    int m_AutoReleaseTime;            //自动释放连接时间，默认为不释放(0)
    bool m_bStartAutoRelease;         //是否已经启动自动释放连接线程，默认为不启动
    std::string m_errInfo;            //错误信息
    pthread_mutex_t m_lock;           // m_pool 互斥锁
    std::vector<PoolClient *> m_pool; // 连接池集合

    public:
    /**
     * 构造函数
     * @param addr {const char*} 服务器监听地址，格式：ip:port
     * @param count {int} 连接池最大连接个数限制
     * @param idx {size_t} 该连接池对象在集合中的下标位置(从 0 开始)
     */
    ConnectPool();

    /**
     * 该类当允许自行销毁时，类实例应为动态对象
     */
    virtual ~ConnectPool();

    /*初始化连接池
     * para @addr 连接地址 格式为 IP:port,IP2:port2
     * para @max 每个地址建立的连接池最大连接数
     * para @preCount 预创建数，>=0 <=count
    */

    bool InitPool(const char *addr, const int max, const int preCount);

    /*释放连接池所有资源*/
    void ReleasePool();

    /**
     * 从连接池中尝试性获取一个连接，当服务器不可用、距上次服务端连接异常时间间隔
     * 未过期或连接池连接个数达到连接上限则将返回 NULL；当创建一个新的与服务器的
     * 连接时失败，则该连接池会被置为不可用状态
     * @return 返回 NULL 说明没有可用连接，否则返回连接句柄
     */
    ConnectInfo *GetConnect();

    /**
     * 归还申请的连接
     * @param conn 已申请的连接
     * @param bConnect 使用的连接是否正常，如果不正常则在回收的时候会重连
     */
    void BackToPool(ConnectInfo *conn, bool bConnect);

    /**
     * 设置连接池的存活状态
     * @param ok {bool} 设置该连接是否正常
     */
    // void set_alive(const char* addr, int port, bool ok /* true | false */);

    /**
     * 设置自动释放连接时间,
     * para @ rTime 自动释放连接时间间隔,单位为秒，必须>=0, =0则不自动释放
     * return  true成功，false 设置失败
     */
    bool SetAutoReleaseTime(const int rTime);

    /**
     * 获取该连接池当前的使用量
     * @return {int}
     */
    int GetUsed() const
    {
        return m_used;
    }

    //获取错误信息
    std::string GetErrInfo()
    {
        return m_errInfo;
    }

    public: //以下公有函数主要提供给CheckAutoRelease使用
    void LockPool();
    void UnLockPool();
    std::vector<PoolClient *> &GetPool();
    int GetAutoReleaseTime();

    private:
    PoolClient *CreatePoolClient(const char *addr, int port);

    int CreateConnect(PoolClient *client);

    //获取一个可用的连接句柄，如果为NULL则没有合适的句柄
    ConnectInfo *GetConnectInfo();
    ConnectInfo *CreateConnectInfo(const std::string &addr, const int port, const int fd);

    //检测长时间不用的连接并关闭
    static void *CheckAutoRelease(void *para);
};

#endif
