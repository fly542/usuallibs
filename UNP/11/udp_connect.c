
int udp_connect(const char * host, const char * serv)
{
    int sockfd=-1, n;
    struct addrinfo hints, *res, * ressave;
    bzero(&hints, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;


    if(0!=(n=getaddrinfo(host, serv, &hints, &res))) {
        return -1;
    }
    ressave = res;

    do {
        sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
        if ( sockfd < 0) {
            continue;
        }
        if(0==connect(sockfd, res->ai_addr, res->ai_addrlen)) {
            break;
        }
        close(sockfd);
        sockfd = -1;
    }while(NULL!=(res=res->ai_next));
    
    freeaddrinfo(ressave);

    return sockfd;
}
