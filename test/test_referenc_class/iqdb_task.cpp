#include "stdafx.h"

#include <stdio.h>

#include "iqdb_task.h"

namespace IQDB {

iqdb_task::pointer create_iqdb_task() {
    iqdb_task * xx = new iqdb_task(200);
    iqdb_task::pointer tmp( xx);
    printf("pointer=%p, xx=%p", &tmp, xx);
    return tmp;
}

iqdb_task::iqdb_task(int a)
: a_(a)
{
    printf("call %s, a=%d\n", __func__, a_);
}

iqdb_task::~iqdb_task() {
    printf("call %s, a=%d\n", __func__, a_);
}

void iqdb_task::test_fun() {
    printf("call %s, a=%d\n", __func__, a_);
}

} /* IQDB  */
