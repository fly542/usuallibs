#ifndef SOCKSERVER_H
#define SOCKSERVER_H

/* pur @ 定义sock相关调用类的基类
 * date @ 2013.10.22
 * author @ haibin.wang
*/

#include "sockbase.h"

class ServerSock : public SockBase {
    public:
    ServerSock();
    virtual ~ServerSock();

    /*
     * pur @ 创建server监听socket
     * para @ port 监听的端口
     * return @ 监听sock句柄
     */
    int CreateServer(const int &port);

    /*
     * pur @ 接受连接
     * para @ timeout 接受的超时时间，单位ms
     * para @ addr 获取连接信息，如果不需要则传递NULL
     * return @ 负值错误,否则返回连接描述符
    */
    int Accept(int timeout, struct sockaddr *addr = NULL);
};
#endif
