num=100
logfile="log/2.log"
valgLog="log/val.log"

echo "" > ${logfile}
echo "" > ${valgLog}
for ((i=0; i<${num}; ++i))
do
    echo "test seq${i}" >> ${logfile}
    echo "" >> ${logfile}

    #./test 1000000 1000000 100 0 >>1.log 
    valgrind --tool=memcheck --leak-check=full --track-origins=yes --show-reachable=yes  --trace-children=yes --log-file=log/tmp_log ./testPool 100 100 1 0 20 >> ${logfile}
    echo "**********      ${i}        ****************" >> ${valgLog}
    echo "" >> ${valgLog}
    echo "" >> ${valgLog}
    cat log/tmp_log >> ${valgLog}
    echo "" >> ${valgLog}
    echo "" >> ${valgLog}
done
